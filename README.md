# Highscore

<a href="https://flathub.org/apps/details/org.gnome.World.Highscore"><img height="51" alt="Download on Flathub" src="https://flathub.org/assets/badges/flathub-badge-en.svg"/> </a>

Highscore is a retro gaming application for the GNOME desktop.

You want to install Highscore if you need a painless way to browse and play your
retro games library, without bothering with advanced features such as
speedrunning tools or video game development tools.

- Website:      https://wiki.gnome.org/Apps/Games
- Issues:       https://gitlab.gnome.org/World/highscore
- Download:     https://download.gnome.org/sources/gnome-games/
- Mailing list: https://mail.gnome.org/mailman/listinfo/games-list
- Matrix room:  https://matrix.to/#/#highscore:gnome.org

## Contribution guide
https://gitlab.gnome.org/World/highscore/blob/main/HACKING.md

## Developing
We support development using Flatpak via GNOME Builder. At the Builder
"Clone..." dialog, enter https://gitlab.gnome.org/World/highscore.git, and the
default build + run development flow using Flatpak should work.
