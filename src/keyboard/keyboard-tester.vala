// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/keyboard/keyboard-tester.ui")]
private class Highscore.KeyboardTester : Adw.Bin {
	[GtkChild]
	private unowned GamepadView gamepad_view;

	private Gtk.EventControllerKey? controller;

	public Retro.KeyJoypadMapping mapping { get; set; }

	private GamepadViewConfiguration _configuration;
	public GamepadViewConfiguration configuration {
		get { return _configuration; }
		construct {
			_configuration = value;
			gamepad_view.configuration = value;
		}
	}

	public KeyboardTester (GamepadViewConfiguration configuration) {
		Object (configuration: configuration);
	}

	public void start () {
		gamepad_view.reset ();
		connect_to_keyboard ();
	}

	public void stop () {
		disconnect_from_keyboard ();
	}

	private void connect_to_keyboard () {
		if (controller != null)
			return;

		controller = new Gtk.EventControllerKey ();

		controller.key_pressed.connect ((keyval, keycode, state) => {
			update_gamepad_view (keycode, true);

			return Gdk.EVENT_STOP;
		});
		controller.key_released.connect ((keyval, keycode, state) => {
			update_gamepad_view (keycode, false);
		});

		add_controller (controller);
	}

	private void disconnect_from_keyboard () {
		if (controller == null)
			return;

		remove_controller (controller);
		controller = null;
	}

	private void update_gamepad_view (uint keycode, bool highlight) {
		int count = Retro.ControllerType.JOYPAD.get_id_count ();
		for (Retro.JoypadId joypad_id = 0; joypad_id < count; joypad_id += 1) {
			if (mapping.get_button_key (joypad_id) == keycode) {
				var code = joypad_id.to_button_code ();
				gamepad_view.highlight ({ EventCode.EV_KEY, code }, highlight);
			}
		}
	}
}
