// This file is part of Highscore. License: GPL-3.0+.

public errordomain Highscore.RunnerError {
	UNSUPPORTED_SYSTEM,
	INVALID_GAME,
	OTHER
}
