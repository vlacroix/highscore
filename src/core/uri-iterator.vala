// This file is part of Highscore. License: GPL-3.0+.

public interface Highscore.UriIterator : Object {
	public abstract new Uri? get ();
	public abstract bool next ();
}
