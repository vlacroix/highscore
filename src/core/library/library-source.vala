// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.LibrarySource : Object, UriSource {
	public string path { get; private set; }

	public LibrarySource (string path) {
		this.path = path;
	}

	public UriIterator iterator () {
		return new LibraryIterator (path);
	}
}
