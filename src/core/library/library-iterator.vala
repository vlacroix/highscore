// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.LibraryIterator : Object, UriIterator {
	private Uri uri;

	private List<FileEnumerator> stack;
	private string[] skip_mime_types;

	public LibraryIterator (string path) {
		stack = new List<FileEnumerator> ();

		skip_mime_types = {};

		var register = PlatformRegister.get_register ();
		var platforms = register.get_all_platforms ();

		foreach (var platform in platforms) {
			if (platform.autodiscovery)
				continue;

			var mime_types = platform.get_mime_types ();

			foreach (var mime in mime_types)
				skip_mime_types += mime;
		}

		var root = File.new_for_path (path);

		try {
			recurse (root);
		} catch (Error e) {
			critical (e.message);
		}
	}

	private void recurse (File dir) throws Error {
		var enumerator = dir.enumerate_children (
			FileAttribute.STANDARD_TYPE + "," +
			FileAttribute.STANDARD_CONTENT_TYPE,
			FileQueryInfoFlags.NONE
		);

		stack.prepend (enumerator);
	}

	public new Uri? get () {
		return uri;
	}

	public bool next () {
		if (stack.length () == 0)
			return false;

		var enumerator = stack.first ().data;

		FileInfo info;
		try {
			while ((info = enumerator.next_file (null)) != null) {
				var child = enumerator.get_child (info);

				if (info.get_file_type () == FileType.DIRECTORY) {
					recurse (child);
					return next ();
				}

				if (info.get_content_type () in skip_mime_types)
					continue;

				uri = new Uri (child.get_uri ());

				return true;

			}
		} catch (Error e) {
			critical (e.message);
		}

		stack.remove (enumerator);

		return stack.length () > 0;
	}
}
