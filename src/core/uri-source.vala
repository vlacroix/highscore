// This file is part of Highscore. License: GPL-3.0+.

public interface Highscore.UriSource : Object {
	public abstract UriIterator iterator ();
}
