// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Game : Object {
	public signal void replaced (Game new_game);

	public string title { get; private set; }
	public Uid uid { get; private set; }
	public Uri uri { get; private set; }
	public Platform platform { get; private set; }
	public MediaSet? media_set { get; set; }
	public bool is_favorite { get; set; }
	public DateTime? last_played { get; set; }

	private Cover game_cover;

	public Game (Uid uid, Uri uri, string title, Platform platform) {
		this.uid = uid;
		this.uri = uri;
		this.title = title;
		this.platform = platform;
	}

	public Cover get_cover () {
		if (game_cover == null)
			game_cover = new DummyCover ();

		return game_cover;
	}

	public void set_cover (Cover cover) {
		game_cover = cover;
	}

	public bool matches_search_terms (string[] search_terms) {
		if (search_terms.length != 0)
			foreach (var term in search_terms)
				if (!(term.casefold () in title.casefold ()))
					return false;

		return true;
	}

	public void update_last_played () {
		last_played = new DateTime.now ();
	}

	public static uint hash (Game key) {
		return Uid.hash (key.uid);
	}

	public static bool equal (Game a, Game b) {
		if (direct_equal (a, b))
			return true;

		return Uid.equal (a.uid, b.uid);
	}

	public static int compare (Game a, Game b) {
		var ret = a.title.collate (b.title);
		if (ret != 0)
			return ret;

		ret = Platform.compare (a.platform, b.platform);
		if (ret != 0)
			return ret;

		return Uid.compare (a.uid, b.uid);
	}

	public static int compare_by_date_time (Game a, Game b) {
		if (a.last_played == null && b.last_played == null)
			return 0;

		if (a.last_played == null)
			return 1;

		if (b.last_played == null)
			return -1;

		return b.last_played.compare (a.last_played);
	}
}
