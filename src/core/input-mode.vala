// This file is part of Highscore. License: GPL-3.0+.

public enum Highscore.InputMode {
	NONE,
	KEYBOARD,
	GAMEPAD,
}
