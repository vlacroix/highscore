// This file is part of Highscore. License: GPL-3.0+.

public interface Highscore.Cover : Object {
	public signal void changed ();

	public abstract File? get_cover ();
	public abstract Gdk.RGBA get_background_color ();
}
