// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Media : Object {
	public string id { get; private set; }
	public string title { get; private set; }

	private Uri[] uris;

	public Media (string id, string title) {
		this.id = id;
		this.title = title;
		this.uris = {};
	}

	public Media.parse (Variant variant) {
		assert (get_variant_type ().equal (variant.get_type ()));

		int child_index = 0;
		var id_child = variant.get_child_value (child_index++);
		var title_child = variant.get_child_value (child_index++);
		var uris_child = variant.get_child_value (child_index++);

		id = id_child.get_string ();
		title = title_child.get_string ();

		uris = {};
		for (int i = 0; i < uris_child.n_children (); i++) {
			var uri = uris_child.get_child_value (i).get_string ();
			uris += new Uri (uri);
		}
	}

	public Uri[] get_uris () {
		return uris;
	}

	public void add_uri (Uri uri) {
		uris += uri;
	}

	public Variant serialize () {
		Variant[] uri_variants = {};
		foreach (var uri in uris)
			uri_variants += new Variant.string (uri.to_string ());

		return new Variant.tuple ({
			new Variant.string (id),
			new Variant.string (title),
			new Variant.array (VariantType.STRING, uri_variants)
		});
	}

	public static VariantType get_variant_type () {
		return new VariantType.tuple ({
			VariantType.STRING,
			VariantType.STRING,
			new VariantType.array (VariantType.STRING)
		});
	}
}
