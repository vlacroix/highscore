// This file is part of Highscore. License: GPL-3.0+.

public delegate void Highscore.GameCallback (Game game);
