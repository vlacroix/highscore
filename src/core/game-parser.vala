// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.GameParser : Object {
	public Platform platform { get; construct; }
	public Uri uri { get; construct; }

	public GameParser (Platform platform, Uri uri) {
		Object (platform: platform, uri: uri);
	}

	public virtual void parse () throws Error {
	}

	public virtual string get_uid () throws Error {
		return Fingerprint.get_uid (uri, platform.get_uid_prefix ());
	}

	public virtual string get_title () {
		return Filename.get_title (uri);
	}

	public virtual Cover? get_cover () {
		return null;
	}

	public virtual string? get_media_id () {
		return null;
	}

	public virtual string? get_media_set_id () {
		return null;
	}

	public virtual MediaSet? create_media_set () throws Error {
		return null;
	}
}
