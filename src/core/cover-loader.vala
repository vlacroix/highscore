// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.CoverLoader : Object {
	const double COVER_BLUR_RADIUS_FACTOR = 30.0 / 128.0;
	const double SHADOW_FACTOR = 20.0 / 128;
	const uint TINY_ICON_SIZE = 32;

	public delegate void CoverReadyCallback (int scale_factor, int cover_size, Gdk.Paintable? cover_paintable);

	private struct CoverRequest {
		Game game;
		int scale_factor;
		int cover_size;
		unowned CoverReadyCallback cb;
	}

	private AsyncQueue<CoverRequest?> request_queue;
	private Thread thread;

	construct {
		request_queue = new AsyncQueue<CoverRequest?> ();
		thread = new Thread<void> (null, run_loader_thread);
	}

	private void run_callback (CoverRequest request, int scale_factor, int cover_size, Gdk.Paintable? cover_paintable) {
		Idle.add (() => {
			request.cb (scale_factor, cover_size, cover_paintable);
			return Source.REMOVE;
		});
	}

	private Gdk.Paintable? try_load_cover (Game game, int size, int scale_factor) {
		var paintable = load_cache_from_disk (game, size, scale_factor, "covers");
		if (paintable != null)
			return paintable;

		var cover = game.get_cover ();

		if (cover.get_cover () == null)
			return null;

		var pixbuf = create_cover_thumbnail (cover, size, scale_factor);
		save_cache_to_disk (game, pixbuf, size, scale_factor, "covers");

		return Gdk.Texture.for_pixbuf (pixbuf);
	}

	private void run_loader_thread () {
		while (true) {
			var request = request_queue.pop ();
			var game = request.game;
			var scale_factor = request.scale_factor;
			var cover_size = request.cover_size;

			var cover_paintable = try_load_cover (game, cover_size, scale_factor);
			if (cover_paintable != null)
				run_callback (request, scale_factor, cover_size, cover_paintable);

			run_callback (request, scale_factor,
			              cover_size, cover_paintable);
		}
	}

	private string get_cache_path (Game game, int size, int scale_factor, string dir_name) {
		var dir = Application.get_image_cache_dir (dir_name, size, scale_factor);

		var uid = game.uid;

		return @"$dir/$uid.png";
	}

	private Gdk.Paintable? load_cache_from_disk (Game game, int size, int scale_factor, string dir) {
		var cache_path = get_cache_path (game, size, scale_factor, dir);

		try {
			return Gdk.Texture.from_file (File.new_for_path (cache_path));
		}
		catch (Error e) {
			return null;
		}
	}

	private void save_cache_to_disk (Game game, Gdk.Pixbuf pixbuf, int size, int scale_factor, string dir_name) {
		Application.try_make_dir (Application.get_image_cache_dir (dir_name, size, scale_factor));
		var now = new GLib.DateTime.now_local ();
		var creation_time = now.to_string ();

		try {
			var cover_cache_path = get_cache_path (game, size, scale_factor, dir_name);
			pixbuf.save (cover_cache_path, "png",
			            "tEXt::Software", "Highscore",
			            "tEXt::Creation Time", creation_time.to_string (),
			            null);
		}
		catch (Error e) {
			critical (e.message);
		}
	}

	private void draw_cover_blur_rect (Cairo.Context cr, Gdk.Pixbuf pixbuf, int size, int scale_factor, bool reverse, int x, int y, int w, int h) {
		int radius = (int) (COVER_BLUR_RADIUS_FACTOR * size);
		int shadow_width = (int) (SHADOW_FACTOR * size);

		if (w == 0 || h == 0)
			return;

		var gradient = new Cairo.Pattern.linear (0, 0,
		                                         h > w ? -shadow_width : 0,
		                                         h < w ? -shadow_width : 0);
		gradient.add_color_stop_rgba (0, 0, 0, 0, 0.15);
		gradient.add_color_stop_rgba (1, 0, 0, 0, 0);

		cr.save ();

		cr.rectangle (0, 0, w, h);
		cr.clip ();

		var subpixbuf = new Gdk.Pixbuf.subpixbuf (pixbuf, x, y, w, h);
		var surface = new Cairo.ImageSurface (Cairo.Format.ARGB32, w, h);
		var surface_cr = new Cairo.Context (surface);
		Gdk.cairo_set_source_pixbuf (surface_cr, subpixbuf, 0, 0);
		surface_cr.paint ();

		CairoBlur.blur_surface (surface, radius);
		cr.set_source_surface (surface, 0, 0);
		cr.paint ();

		if (reverse)
			cr.rotate (Math.PI);
		else if (h > w)
			cr.translate (w, 0);
		else
			cr.translate (0, h);

		cr.set_source (gradient);
		cr.paint ();

		cr.rotate (Math.PI);

		if (h > w)
			cr.rectangle (0, reverse ? 0 : -h, scale_factor, h);
		else
			cr.rectangle (reverse ? 0 : -w, 0, w, scale_factor);

		cr.set_source_rgba (0, 0, 0, 0.2);
		cr.fill ();

		cr.set_source_rgba (0, 0, 0, 0.1);
		cr.paint ();

		cr.restore ();
	}

	private Gdk.Pixbuf? create_cover_thumbnail (Cover cover, int size, int scale_factor) {
		var file = cover.get_cover ();
		Gdk.Pixbuf overlay_pixbuf, blur_pixbuf;
		int overlay_x, overlay_y;
		int width, height, zoom_width, zoom_height;
		double aspect_ratio;
		int natural_size = size * scale_factor;
		bool is_tiny;

		assert (file != null);

		Gdk.Pixbuf.get_file_info (file.get_path (), out width, out height);

		aspect_ratio = (double) width / height;
		is_tiny = width <= TINY_ICON_SIZE && height <= TINY_ICON_SIZE;

		if (height >= width) {
			height = natural_size;
			width = (int) (natural_size * aspect_ratio);

			zoom_width = natural_size;
			zoom_height = (int) (natural_size * height / (double) width);

			overlay_x = (int) ((height - width) / 2);
			overlay_y = 0;
		}
		else {
			width = natural_size;
			height = (int) (natural_size / aspect_ratio);

			zoom_height = natural_size;
			zoom_width = (int) (natural_size * width / (double) height);

			overlay_x = 0;
			overlay_y = (int) ((width - height) / 2);
		}

		var image_surface = new Cairo.ImageSurface (Cairo.Format.ARGB32, natural_size, natural_size);
		var cr = new Cairo.Context (image_surface);

		try {
			var pixbuf = new Gdk.Pixbuf.from_file (file.get_path ());
			var interpolation = is_tiny ? Gdk.InterpType.NEAREST :
			                              Gdk.InterpType.HYPER;
			overlay_pixbuf = pixbuf.scale_simple (width, height, interpolation);
			blur_pixbuf = pixbuf.scale_simple (zoom_width, zoom_height, Gdk.InterpType.HYPER);
		}
		catch (Error e) {
			critical ("Failed to load cover image: %s", e.message);
			return null;
		}

		cr.save ();

		var rgba = cover.get_background_color ();
		cr.set_source_rgb (rgba.red, rgba.green, rgba.blue);
		cr.rectangle (0, 0, natural_size, natural_size);
		cr.fill ();

		cr.restore ();

		cr.save ();

		if (height > width) {
			var blur_y = (int) ((double) (height - width) / 2);

			draw_cover_blur_rect (cr, blur_pixbuf, natural_size, scale_factor, false,
			                      0, blur_y, overlay_x, natural_size);

			if (height > width)
				cr.translate (overlay_x + width, 0);
			else
				cr.translate (0, blur_y);

			draw_cover_blur_rect (cr, blur_pixbuf, natural_size, scale_factor, true,
			                      overlay_x + width, blur_y,
			                      natural_size - width - overlay_x, natural_size);
		}
		else if (height < width) {
			var blur_x = (int) ((double) (width - height) / 2);

			draw_cover_blur_rect (cr, blur_pixbuf, natural_size, scale_factor, false,
			                      blur_x, 0, natural_size, overlay_y);

			if (height > width)
				cr.translate (blur_x, 0);
			else
				cr.translate (0, overlay_y + height);

			draw_cover_blur_rect (cr, blur_pixbuf, natural_size, scale_factor, true,
			                      blur_x, overlay_y + height,
			                      natural_size, natural_size - height - overlay_y);
		}

		cr.restore ();

		Gdk.cairo_set_source_pixbuf (cr, overlay_pixbuf, overlay_x, overlay_y);
		cr.paint ();

		return Gdk.pixbuf_get_from_surface (image_surface, 0, 0, natural_size, natural_size);
	}

	public void fetch_cover (Game game, int scale_factor, int cover_size, CoverReadyCallback cb) {
		request_queue.push ({ game, scale_factor, cover_size, cb });
	}
}
