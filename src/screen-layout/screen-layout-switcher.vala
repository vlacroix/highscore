// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/screen-layout/screen-layout-switcher.ui")]
public class Highscore.ScreenLayoutSwitcher : Gtk.Box, HeaderBarWidget {
	[GtkChild]
	private unowned Gtk.Revealer change_screen_revealer;
	[GtkChild]
	private unowned Gtk.Button change_screen_button;
	[GtkChild]
	private unowned Gtk.MenuButton layout_button;
	[GtkChild]
	private unowned Gtk.Popover layout_popover;
	[GtkChild]
	private unowned Gtk.ListBox list_box;

	private HashTable<ScreenLayout, ScreenLayoutItem> items;

	public ScreenLayout screen_layout { get; set; }
	public bool view_bottom_screen { get; set; }

	public bool popover_visible {
		get { return layout_popover.visible; }
	}

	static construct {
		install_action ("switcher.select-top-bottom", null, widget => {
			var self = widget as ScreenLayoutSwitcher;

			self.screen_layout = ScreenLayout.TOP_BOTTOM;
		});

		install_action ("switcher.select-left-right", null, widget => {
			var self = widget as ScreenLayoutSwitcher;

			self.screen_layout = ScreenLayout.LEFT_RIGHT;
		});

		install_action ("switcher.select-right-left", null, widget => {
			var self = widget as ScreenLayoutSwitcher;

			self.screen_layout = ScreenLayout.RIGHT_LEFT;
		});

		install_action ("switcher.select-quick-switch", null, widget => {
			var self = widget as ScreenLayoutSwitcher;

			self.screen_layout = ScreenLayout.QUICK_SWITCH;
		});

		install_action ("switcher.view-top-screen", null, widget => {
			var self = widget as ScreenLayoutSwitcher;

			self.view_bottom_screen = false;
		});

		install_action ("switcher.view-bottom-screen", null, widget => {
			var self = widget as ScreenLayoutSwitcher;

			self.view_bottom_screen = true;
		});
	}

	public override void constructed () {
		items = new HashTable<ScreenLayout, ScreenLayoutItem> (direct_hash, direct_equal);
		foreach (var layout in ScreenLayout.get_layouts ()) {
			var item = new ScreenLayoutItem (layout);

			items[layout] = item;
			list_box.append (item);
		}

		update_ui ();

		notify["screen-layout"].connect (update_ui);
		notify["view-bottom-screen"].connect (update_ui);

		base.constructed ();
	}

	[GtkCallback]
	private void on_menu_state_changed () {
		notify_property ("popover-visible");
	}

	[GtkCallback]
	private void update_ui () {
		layout_button.icon_name = screen_layout.get_icon ();

		foreach (var item in items.get_values ())
			item.selected = item.layout == screen_layout;

		var item = items[screen_layout];
		list_box.select_row (item);

		change_screen_revealer.reveal_child = (screen_layout == ScreenLayout.QUICK_SWITCH);
		change_screen_button.icon_name = view_bottom_screen ?
		                                 "view-top-screen-symbolic" :
		                                 "view-bottom-screen-symbolic";

		action_set_enabled ("switcher.select-top-screen", screen_layout == QUICK_SWITCH);
		action_set_enabled ("switcher.select-bottom-screen", screen_layout == QUICK_SWITCH);
	}

	[GtkCallback]
	private void on_screen_changed () {
		view_bottom_screen = !view_bottom_screen;
	}

	[GtkCallback]
	private void on_row_activated (Gtk.ListBoxRow row) {
		var layout_item = row as ScreenLayoutItem;

		screen_layout = layout_item.layout;

		layout_popover.popdown ();
	}
}
