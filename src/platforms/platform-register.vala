// This file is part of Highscore. License: GPL-3.0+.

private class Highscore.PlatformRegister : Object {
	private static PlatformRegister instance;
	private HashTable<string, Platform> platforms;

	private PlatformRegister () {
		platforms = new HashTable<string, Platform> (str_hash, str_equal);

		init_platforms ();
	}

	public static PlatformRegister get_register () {
		if (instance == null)
			instance = new PlatformRegister ();

		return instance;
	}

	public void add_platform (Platform platform) {
		var platform_id = platform.get_id ();

		assert (!platforms.contains (platform_id));

		platforms[platform_id] = platform;
	}

	public List<Platform> get_all_platforms () {
		var values = platforms.get_values ();

		var result = new List<Platform> ();
		foreach (var platform in values)
			result.prepend (platform);

		result.sort (Platform.compare);

		return result;
	}

	public Platform? get_platform (string id) {
		return platforms[id];
	}

	private void init_platforms () {
		var platform = new Platform (
			"Amiga",
			_("Amiga"),
			"application/x-amiga-disk-format",
			"amiga"
		);
		add_platform (platform);

		platform = new Platform (
			"Atari2600",
			_("Atari 2600"),
			"application/x-atari-2600-rom",
			"atari-2600"
		);
		add_platform (platform);

		platform = new Platform (
			"Atari7800",
			_("Atari 7800"),
			"application/x-atari-7800-rom",
			"atari-7800"
		);
		add_platform (platform);

		platform = new Platform (
			"AtariLynx",
			_("Atari Lynx"),
			"application/x-atari-lynx-rom",
			"atari-lynx"
		);
		add_platform (platform);

		platform = new Platform.with_mime_types (
			"Dreamcast",
			_("Dreamcast"),
			{ "application/x-gd-rom-cue", "application/x-discjuggler-cd-image" },
			"application/x-dreamcast-rom",
			"dreamcast"
		);
		platform.parser_type = typeof (DreamcastParser);
		add_platform (platform);

		platform = new Platform (
			"FamicomDiskSystem",
			/* translators: only released in eastern Asia */
			_("Famicom Disk System"),
			"application/x-fds-disk",
			"fds"
		);
		add_platform (platform);

		platform = new Platform (
			"GameBoy",
			_("Game Boy"),
			"application/x-gameboy-rom",
			"game-boy"
		);
		add_platform (platform);

		platform = new Platform (
			"GameBoyColor",
			_("Game Boy Color"),
			"application/x-gameboy-color-rom",
			/* The prefix is the same as the Game Boy type for backward compatibility */
			"game-boy"
		);
		add_platform (platform);

		platform = new Platform (
			"GameBoyAdvance",
			_("Game Boy Advance"),
			"application/x-gba-rom",
			"game-boy-advance"
		);
		add_platform (platform);

		platform = new Platform (
			"GameCube",
			_("Nintendo GameCube"),
			"application/x-gamecube-rom",
			"game-cube"
		);
		platform.parser_type = typeof (GameCubeParser);
		platform.runner_type = typeof (GameCubeRunner);
		add_platform (platform);

		platform = new Platform (
			"GameGear",
			_("Game Gear"),
			"application/x-gamegear-rom",
			"game-gear"
		);
		add_platform (platform);

		platform = new Platform.with_mime_types (
			"MAME",
			_("Arcade"),
			{ "application/zip" },
			"application/x-mame-rom",
			"mame"
		);
		platform.parser_type = typeof (MameParser);
		add_platform (platform);

		platform = new Platform (
			"MasterSystem",
			/* translators: also known as "Sega Mark III" in eastern Asia */
			_("Master System"),
			"application/x-sms-rom",
			"master-system"
		);
		add_platform (platform);

		platform = new Platform (
			"MSDOS",
			_("MS-DOS"),
			"application/x-ms-dos-executable",
			"ms-dos"
		);
		platform.autodiscovery = false;
		platform.runner_type = typeof (MsDosRunner);
		add_platform (platform);

		platform = new Platform (
			"NeoGeoPocket",
			_("Neo Geo Pocket"),
			"application/x-neo-geo-pocket-rom",
			"neo-geo-pocket"
		);
		add_platform (platform);

		platform = new Platform (
			"NeoGeoPocketColor",
			_("Neo Geo Pocket Color"),
			"application/x-neo-geo-pocket-color-rom",
			 /* The prefix is the same as the Neo Geo Pocket type for backward compatibility */
			"neo-geo-pocket"
		);
		add_platform (platform);

		platform = new Platform.with_mime_types (
			"Nintendo3DS",
			_("Nintendo 3DS"),
			{ "application/x-nintendo-3ds-rom", "application/x-nintendo-3ds-executable" },
			"application/x-nintendo-3ds-rom",
			"nintendo-3ds"
		);
		platform.snapshot_type = typeof (Nintendo3DsSnapshot);
		platform.runner_type = typeof (Nintendo3DsRunner);
		add_platform (platform);

		platform = new Platform (
			"Nintendo64",
			_("Nintendo 64"),
			"application/x-n64-rom",
			"nintendo-64"
		);
		platform.snapshot_type = typeof (Nintendo64Snapshot);
		platform.runner_type = typeof (Nintendo64Runner);
		add_platform (platform);

		platform = new Platform (
			"NintendoDS",
			_("Nintendo DS"),
			"application/x-nintendo-ds-rom",
			"nintendo-ds"
		);
		platform.snapshot_type = typeof (NintendoDsSnapshot);
		platform.runner_type = typeof (NintendoDsRunner);
		platform.parser_type = typeof (NintendoDsParser);
		add_platform (platform);

		platform = new Platform (
			"NintendoEntertainmentSystem",
			/* translators: known as "Famicom" in eastern Asia */
			_("Nintendo Entertainment System"),
			"application/x-nes-rom",
			"nes"
		);
		add_platform (platform);

		platform = new Platform.with_mime_types (
			"PlayStation",
			_("PlayStation"),
			{ "application/x-cue" },
			"application/x-playstation-rom",
			"playstation"
		);
		platform.parser_type = typeof (PlayStationParser);
		add_platform (platform);

		platform = new Platform.with_mime_types (
			"SegaCD",
			/* translators: known as "Mega-CD" in most of the world */
			_("Sega CD"),
			{ "application/x-cue", "application/x-sega-cd-rom" },
			"application/x-sega-cd-rom",
			"mega-cd"
		);
		platform.parser_type = typeof (SegaCDParser);
		add_platform (platform);

		platform = new Platform.with_mime_types (
			"SegaCD32X",
			/* translators: known as "Mega-CD 32X" in most of the world */
			_("Sega CD 32X"),
			{ "application/x-cue", "application/x-sega-cd-rom", "application/x-genesis-32x-rom" },
			"application/x-sega-cd-rom",
			"mega-cd"
		);
		platform.parser_type = typeof (SegaCDParser);
		add_platform (platform);

		platform = new Platform (
			"Sega32X",
			/* translators: known as "Mega Drive 32X", "Mega 32X" or "Super 32X" in other places */
			_("Genesis 32X"),
			"application/x-genesis-32x-rom",
			"mega-drive-32x"
		);
		add_platform (platform);

		platform = new Platform (
			"SegaGenesis",
			/* translators: known as "Mega Drive" in most of the world */
			_("Sega Genesis"),
			"application/x-genesis-rom",
			"mega-drive"
		);
		add_platform (platform);

		platform = new Platform.with_mime_types (
			"SegaSaturn",
			_("Sega Saturn"),
			{ "application/x-cue", "application/x-saturn-rom" },
			"application/x-saturn-rom",
			"sega-saturn"
		);
		platform.parser_type = typeof (SegaSaturnParser);
		add_platform (platform);

		platform = new Platform (
			"SegaPico",
			_("Sega Pico"),
			"application/x-sega-pico-rom",
			"sega-pico"
		);
		add_platform (platform);

		platform = new Platform (
			"SG1000",
			_("SG-1000"),
			"application/x-sg1000-rom",
			"sg-1000"
		);
		add_platform (platform);

		platform = new Platform (
			"SuperNintendoEntertainmentSystem",
			/* translators: known as "Super Famicom" in eastern Asia */
			_("Super Nintendo Entertainment System"),
			"application/vnd.nintendo.snes.rom",
			"snes"
		);
		add_platform (platform);

		platform = new Platform (
			"TurboGrafx16",
			/* translators: known as "PC Engine" in eastern Asia and France */
			_("TurboGrafx-16"),
			"application/x-pc-engine-rom",
			"pc-engine"
		);
		add_platform (platform);

		platform = new Platform.with_mime_types (
			"TurboGrafxCD",
			/* translators: known as "CD-ROM²" in eastern Asia and France */
			_("TurboGrafx-CD"),
			{ "application/x-cue" },
			"application/x-pc-engine-cd-rom",
			"pc-engine"
		);
		platform.parser_type = typeof (TurboGrafxCDParser);
		add_platform (platform);

		platform = new Platform (
			"VirtualBoy",
			_("Virtual Boy"),
			"application/x-virtual-boy-rom",
			"virtual-boy"
		);
		platform.parser_type = typeof (VirtualBoyParser);
		add_platform (platform);

		platform = new Platform (
			"Wii",
			_("Wii"),
			"application/x-wii-rom",
			"wii"
		);
		platform.parser_type = typeof (WiiParser);
		add_platform (platform);

		platform = new Platform (
			"WiiWare",
			_("WiiWare"),
			"application/x-wii-wad",
			"wii-ware"
		);
		add_platform (platform);

		platform = new Platform (
			"WonderSwan",
			_("WonderSwan"),
			"application/x-wonderswan-rom",
			"wonderswan"
		);
		add_platform (platform);

		platform = new Platform (
			"WonderSwanColor",
			_("WonderSwan Color"),
			"application/x-wonderswan-color-rom",
			"wonderswan-color"
		);
		add_platform (platform);
	}
}
