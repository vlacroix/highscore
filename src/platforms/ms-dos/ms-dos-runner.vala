// This file is part of Highscore. License: GPL-3.0+.

private class Highscore.MsDosRunner : Runner {
	public MsDosRunner (Game game, CoreSource source) {
		base (game, source);
	}

	construct {
		input_capabilities = new InputCapabilities (true, false);
	}
}
