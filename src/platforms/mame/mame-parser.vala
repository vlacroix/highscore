// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.MameParser : GameParser {
	private string uid;
	private string title;

	public MameParser (Platform platform, Uri uri) {
		base (platform, uri);
	}

	public override void parse () throws Error {
		var supported_games = MameGameInfo.get_supported_games ();

		var file = uri.to_file ();
		var game_id = file.get_basename ();
		game_id = /\.zip$/.replace (game_id, game_id.length, 0, "");

		if (!supported_games.contains (game_id))
			throw new MameError.INVALID_GAME_ID ("Invalid MAME game id “%s” for “%s”.", game_id, uri.to_string ());

		uid = @"mame-$game_id".down ();

		title = supported_games[game_id];
		title = title.split ("(")[0];
		title = title.strip ();
	}

	public override string get_uid () {
		return uid;
	}

	public override string get_title () {
		return title;
	}
}
