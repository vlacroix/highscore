// This file is part of Highscore. License: GPL-3.0+.

errordomain Highscore.MameError {
	INVALID_GAME_ID,
}
