// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.NintendoDsParser : GameParser {
	private Cover cover;

	public NintendoDsParser (Platform platform, Uri uri) {
		base (platform, uri);
	}

	public override void parse () throws Error {
		cover = new NintendoDsCover (uri);
	}

	public override Cover? get_cover () {
		return cover;
	}
}
