// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.NintendoDsCover : Object, Cover {
	private Uri uri;
	private bool extracted;
	private File file;

	public NintendoDsCover (Uri uri) {
		this.uri = uri;
		extracted = false;
	}

	private static extern Gdk.Pixbuf extract (string uri) throws Error;

	public File? get_cover () {
		if (extracted)
			return file;

		extracted = true;

		try {
			FileIOStream stream;
			file = File.new_tmp ("nintendo_ds_cover_XXXXXX", out stream);
			var pixbuf = extract (uri.to_string ());
			pixbuf.save_to_stream (stream.get_output_stream (), "png");
		}
		catch (Error e) {
			warning (e.message);
		}

		return file;
	}

	public Gdk.RGBA get_background_color () {
		// The color is about the mean value of the gradients on which icons are
		// drawn in the Nintendo DS and DSi.
		return { 0.85f, 0.85f, 0.85f, 1.0f };
	}
}
