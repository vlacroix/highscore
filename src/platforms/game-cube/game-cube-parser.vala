// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.GameCubeParser : GameParser {
	private string uid;

	public GameCubeParser (Platform platform, Uri uri) {
		base (platform, uri);
	}

	public override void parse () throws Error {
		var file = uri.to_file ();
		var header = new GameCubeHeader (file);
		header.check_validity ();

		var prefix = platform.get_uid_prefix ();
		var game_id = header.get_game_id ();

		uid = @"$prefix-$game_id".down ();
	}

	public override string get_uid () {
		return uid;
	}
}
