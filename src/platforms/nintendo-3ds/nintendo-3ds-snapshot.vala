// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Nintendo3DsSnapshot : Snapshot {
	public ScreenLayout screen_layout { get; set; }
	public bool view_bottom_screen { get; set; }

	protected override void load_metadata (KeyFile keyfile) throws KeyFileError {
		base.load_metadata (keyfile);

		var layout_value = keyfile.get_string ("Nintendo 3DS", "Screen Layout");
		view_bottom_screen = keyfile.get_boolean ("Nintendo 3DS", "View Bottom Screen");

		screen_layout = Nintendo3DsLayout.from_value (layout_value);
	}

	protected override void save_metadata (KeyFile keyfile) {
		base.save_metadata (keyfile);

		keyfile.set_string ("Nintendo 3DS", "Screen Layout", Nintendo3DsLayout.get_value (screen_layout));
		keyfile.set_boolean ("Nintendo 3DS", "View Bottom Screen", view_bottom_screen);
	}
}
