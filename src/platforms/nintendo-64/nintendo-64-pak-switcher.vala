// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/platforms/nintendo-64/nintendo-64-pak-switcher.ui")]
private class Highscore.Nintendo64PakSwitcher : Adw.Bin, HeaderBarWidget {
	[GtkChild]
	private unowned Gtk.PopoverMenu menu_popover;

	public Nintendo64Runner runner { get; construct; }

	public Nintendo64Pak pak1 { get; set; }
	public Nintendo64Pak pak2 { get; set; }
	public Nintendo64Pak pak3 { get; set; }
	public Nintendo64Pak pak4 { get; set; }

	public bool popover_visible {
		get { return menu_popover.visible; }
	}

	private Menu menu;

	public override void constructed () {
		menu = new Menu ();
		menu_popover.menu_model = menu;

		update_ui ();

		runner.controllers_changed.connect (update_ui);

		bind_property ("pak1", runner,
		               "pak1", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
		bind_property ("pak2", runner,
		               "pak2", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
		bind_property ("pak3", runner,
		               "pak3", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
		bind_property ("pak4", runner,
		               "pak4", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);

		base.constructed ();
	}

	public Nintendo64PakSwitcher (Nintendo64Runner runner) {
		Object (runner: runner);
	}

	static construct {
		install_property_action ("n64.set_pak1", "pak1");
		install_property_action ("n64.set_pak2", "pak2");
		install_property_action ("n64.set_pak3", "pak3");
		install_property_action ("n64.set_pak4", "pak4");
	}

	[GtkCallback]
	private void on_menu_state_changed () {
		notify_property ("popover-visible");
	}

	private void update_ui () {
		menu.remove_all ();

		var core = runner.get_core ();
		var iterator = core.iterate_controllers ();

		uint n_players = 0;

		uint port;
		unowned Retro.Controller controller;
		while (iterator.next (out port, out controller)) {
			if (n_players > 3)
				break;

			n_players++;
		}

		iterator = core.iterate_controllers ();

		uint i = 0;

		while (iterator.next (out port, out controller)) {
			if (i > n_players)
				break;

			i++;

			var section = new Menu ();

			var item = new MenuItem (_("Controller Pak"), "n64.set_pak");
			item.set_action_and_target (@"n64.set_pak$i", "s", "memory");
			section.append_item (item);

			item = new MenuItem (_("Rumble Pak"), "n64.set_pak");

			if (controller.get_supports_rumble ()) {
				item.set_action_and_target (@"n64.set_pak$i", "s", "rumble");
			} else {
				// We just want to disable the item
				item.set_detailed_action ("n64.empty");
			}

			section.append_item (item);

			if (n_players > 1)
				menu.append_section (_("Player %u").printf (i), section);
			else
				menu.append_section (null, section);
		}
	}
}
