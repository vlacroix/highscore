// This file is part of Highscore. License: GPL-3.0+.

#ifndef DISC_IMAGE_TIME_H
#define DISC_IMAGE_TIME_H

#include <glib.h>

G_BEGIN_DECLS

typedef struct _HighscoreDiscImageTime HighscoreDiscImageTime;

struct _HighscoreDiscImageTime {
  guint8 minute;
  guint8 second;
  guint8 frame;
};

void highscore_disc_image_time_set_minute_second_frame (HighscoreDiscImageTime *time,
                                                        guint8                  minute,
                                                        guint8                  second,
                                                        guint8                  frame);
void highscore_disc_image_time_set_from_time_reference (HighscoreDiscImageTime *time,
                                                        guint8                 *time_reference);
gint highscore_disc_image_time_get_sector (const HighscoreDiscImageTime *time);
void highscore_disc_image_time_increment (HighscoreDiscImageTime *time);

G_END_DECLS

#endif /* DISC_IMAGE_TIME_H */
