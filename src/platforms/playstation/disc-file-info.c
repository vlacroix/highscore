// This file is part of Highscore. License: GPL-3.0+.

#include "disc-file-info.h"

#include <string.h>
#include <stdio.h>
#include <gio/gio.h>

/* Private */

static gboolean
highscore_disc_file_info_is_valid (const HighscoreDiscFileInfo *file_info)
{
  // FIXME Magic number, I have no idea what it is but it works.
  const gsize MAGIC_SIZE = 47;

  g_return_val_if_fail (file_info != NULL, FALSE);

  return file_info->length >= MAGIC_SIZE + file_info->name_length;
}

static HighscoreDiscFileInfo *
highscore_disc_file_info_get_next (const HighscoreDiscFileInfo *file_info)
{
  g_return_val_if_fail (file_info != NULL, NULL);

  if (!highscore_disc_file_info_is_valid (file_info))
    return NULL;

  return (HighscoreDiscFileInfo *) ((gpointer) file_info + file_info->length);
}

/* Public */

gboolean
highscore_disc_file_info_is_directory (HighscoreDiscFileInfo *file_info)
{
  g_return_val_if_fail (file_info != NULL, FALSE);

  return file_info->flags & 0x2;
}

gchar *
highscore_disc_file_info_access_name (HighscoreDiscFileInfo *file_info)
{
  g_return_val_if_fail (file_info != NULL, NULL);

  return (gchar *) file_info + sizeof (HighscoreDiscFileInfo);
}

void
highscore_disc_file_info_foreach_file (HighscoreDiscFileInfo                *file_info,
                                       gsize                                 size,
                                       HighscoreDiscFileInfoForeachCallback  callback,
                                       gpointer                              user_data)
{
  HighscoreDiscFileInfo *current;

  g_return_if_fail (file_info != NULL);

  for (current = file_info; current != NULL && highscore_disc_file_info_is_valid (current); current = highscore_disc_file_info_get_next (current)) {
    // The file info should never go beyond the end of the buffer.
    if ((gpointer) current - (gpointer) file_info + sizeof (HighscoreDiscFileInfo) >= size ||
        (gpointer) current - (gpointer) file_info + current->length >= size)
      break;

    if (!callback (current, user_data))
      break;
  }
}
