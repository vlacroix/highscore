// This file is part of Highscore. License: GPL-3.0+.

#ifndef DISC_IMAGE_H
#define DISC_IMAGE_H

#include <gio/gio.h>
#include <glib.h>

#include "disc-file-info.h"
#include "disc-image-time.h"

G_BEGIN_DECLS

typedef struct _HighscoreDiscFrameMode1 HighscoreDiscFrameMode1;

struct _HighscoreDiscFrameMode1 {
  guint8 synchronization[12];
  guint8 header[12];
  guint8 content[2048];
  guint8 error_correction_code[280];
};

typedef struct _HighscoreDiscFrameMode2 HighscoreDiscFrameMode2;

struct _HighscoreDiscFrameMode2 {
  guint8 synchronization[12];
  guint8 content[2340];
};

typedef union _HighscoreDiscFrame HighscoreDiscFrame;

union _HighscoreDiscFrame {
  HighscoreDiscFrameMode1 mode1;
  HighscoreDiscFrameMode2 mode2;
};

typedef struct _HighscoreDiscImage HighscoreDiscImage;

struct _HighscoreDiscImage {
  GFileInputStream *input_stream;
};

void highscore_disc_image_open (HighscoreDiscImage  *disc,
                                const char          *filename,
                                GError             **error);
void highscore_disc_image_dispose (HighscoreDiscImage *disc);
gboolean highscore_disc_image_read_frame (HighscoreDiscImage            *disc,
                                          const HighscoreDiscImageTime  *time,
                                          HighscoreDiscFrame            *frame,
                                          GCancellable                  *cancellable,
                                          GError                       **error);
gboolean highscore_disc_image_read_directory (HighscoreDiscImage      *disc,
                                              HighscoreDiscImageTime  *time,
                                              guint8                  *dst,
                                              GCancellable            *cancellable,
                                              GError                 **error);
gboolean highscore_disc_image_get_file (HighscoreDiscImage      *disc,
                                        HighscoreDiscFileInfo   *file_info,
                                        const gchar             *filename,
                                        HighscoreDiscImageTime  *time,
                                        GCancellable            *cancellable,
                                        GError                 **error);

G_END_DECLS

#endif /* DISC_IMAGE_H */
