// This file is part of Highscore. License: GPL-3.0+.

errordomain Highscore.PlayStationError {
	INVALID_HEADER,
	INVALID_FILE_TYPE,
	INVALID_CUE_SHEET,
}
