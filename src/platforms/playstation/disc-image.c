// This file is part of Highscore. License: GPL-3.0+.

#include "disc-image.h"

#include <string.h>
#include "disc-file-info.h"
#include "disc-image-time.h"

/* Private */

#define HIGHSCORE_DISC_IMAGE_ERROR highscore_disc_image_error_quark ()

#define HIGHSCORE_DISC_IMAGE_FRAME_SIZE           2352
#define HIGHSCORE_DISC_IMAGE_FRAME_HEADER_SIZE    12

enum HighscoreDiscImageError {
  HIGHSCORE_DISC_IMAGE_ERROR_INVALID_SECTOR,
};

typedef struct {
  const gchar        *filename;
  HighscoreDiscImageTime *time;
  gboolean            is_dir;
  gboolean            found;
} GetFileData;

static gboolean
get_file_co (HighscoreDiscFileInfo *file_info,
             gpointer           user_data)
{
  GetFileData *data = (GetFileData *) user_data;

  if (highscore_disc_file_info_is_directory (file_info)) {
    if (g_ascii_strncasecmp (highscore_disc_file_info_access_name (file_info), data->filename, file_info->name_length) == 0) {
      if (data->filename[file_info->name_length] != '\\')
        return TRUE;

      data->filename += file_info->name_length + 1;

      highscore_disc_image_time_set_from_time_reference (data->time, file_info->extent);
      data->is_dir = TRUE;
      data->found = TRUE;

      return FALSE;
    }
  }
  else {
    if (g_ascii_strncasecmp (highscore_disc_file_info_access_name (file_info), data->filename, strlen (data->filename)) == 0) {
      highscore_disc_image_time_set_from_time_reference (data->time, file_info->extent);
      data->is_dir = FALSE;
      data->found = TRUE;

      return FALSE;
    }
  }

  return TRUE;
}

static GQuark
highscore_disc_image_error_quark (void)
{
  return g_quark_from_static_string ("highscore-disc-image-error-quark");
}

/* Public */

void
highscore_disc_image_open (HighscoreDiscImage  *disc,
                           const char          *filename,
                           GError             **error)
{
  GFile *file;
  GError *tmp_error = NULL;

  file = g_file_new_for_path (filename);
  g_clear_object (&disc->input_stream);
  disc->input_stream = g_file_read (file, NULL, &tmp_error);
  if (tmp_error != NULL) {
    g_propagate_error (error, tmp_error);
    g_object_unref(file);

    return;
  }

  g_object_unref(file);
}

void
highscore_disc_image_dispose (HighscoreDiscImage *disc)
{
  g_clear_object (&disc->input_stream);
}

gboolean
highscore_disc_image_read_frame (HighscoreDiscImage            *disc,
                                 const HighscoreDiscImageTime  *time,
                                 HighscoreDiscFrame            *frame,
                                 GCancellable                  *cancellable,
                                 GError                       **error)
{
  gssize read;
  gint sector;
  gsize offset;
  GError *tmp_error = NULL;

  g_return_val_if_fail (disc != NULL, FALSE);
  g_return_val_if_fail (time != NULL, FALSE);
  g_return_val_if_fail (frame != NULL, FALSE);

  sector = highscore_disc_image_time_get_sector (time);
  if (sector < 0) {
    g_set_error (error,
                 HIGHSCORE_DISC_IMAGE_ERROR,
                 HIGHSCORE_DISC_IMAGE_ERROR_INVALID_SECTOR,
                 "The sector index %d is inferior to 0 and hence is invalid.",
                 sector);

    return FALSE;
  }

  if (!g_size_checked_mul (&offset, sector, sizeof (HighscoreDiscFrame))) {
    g_set_error (error,
                 HIGHSCORE_DISC_IMAGE_ERROR,
                 HIGHSCORE_DISC_IMAGE_ERROR_INVALID_SECTOR,
                 "The sector index %d is too big to be usable and hence is invalid.",
                 sector);

    return FALSE;
  }

  g_seekable_seek (G_SEEKABLE (disc->input_stream),
                   offset, G_SEEK_SET,
                   cancellable, &tmp_error);
  if (tmp_error != NULL) {
    g_propagate_error (error, tmp_error);

    return FALSE;
  }

  read = g_input_stream_read (G_INPUT_STREAM (disc->input_stream),
                              frame, sizeof (HighscoreDiscFrame),
                              cancellable, &tmp_error);
  if (tmp_error != NULL) {
    g_propagate_error (error, tmp_error);

    return FALSE;
  }

  return read == sizeof (HighscoreDiscFrame);
}

gboolean
highscore_disc_image_read_directory (HighscoreDiscImage      *disc,
                                     HighscoreDiscImageTime  *time,
                                     guint8                  *dst,
                                     GCancellable            *cancellable,
                                     GError                 **error)
{
  gssize read;
  gint sector;
  GError *tmp_error = NULL;

  sector = highscore_disc_image_time_get_sector(time);
  g_seekable_seek (G_SEEKABLE (disc->input_stream),
                   sector * HIGHSCORE_DISC_IMAGE_FRAME_SIZE + HIGHSCORE_DISC_IMAGE_FRAME_HEADER_SIZE + 12,
                   G_SEEK_SET, cancellable, &tmp_error);
  if (tmp_error != NULL) {
    g_propagate_error (error, tmp_error);

    return FALSE;
  }

  read = g_input_stream_read (G_INPUT_STREAM (disc->input_stream),
                              dst, 2048,
                              cancellable, &tmp_error);
  if (tmp_error != NULL) {
    g_propagate_error (error, tmp_error);

    return FALSE;
  }

  if (read == -1)
    return FALSE;

  highscore_disc_image_time_increment (time);

  sector = highscore_disc_image_time_get_sector(time);
  g_seekable_seek (G_SEEKABLE (disc->input_stream),
                   sector * HIGHSCORE_DISC_IMAGE_FRAME_SIZE + HIGHSCORE_DISC_IMAGE_FRAME_HEADER_SIZE + 12,
                   G_SEEK_SET, cancellable, &tmp_error);
  if (tmp_error != NULL) {
    g_propagate_error (error, tmp_error);

    return FALSE;
  }

  read = g_input_stream_read (G_INPUT_STREAM (disc->input_stream),
                              dst + 2048, 2048,
                              cancellable, &tmp_error);
  if (tmp_error != NULL) {
    g_propagate_error (error, tmp_error);

    return FALSE;
  }

  if (read == -1)
    return FALSE;

  return TRUE;
}

gboolean
highscore_disc_image_get_file (HighscoreDiscImage      *disc,
                               HighscoreDiscFileInfo   *file_info,
                               const gchar             *filename,
                               HighscoreDiscImageTime  *time,
                               GCancellable            *cancellable,
                               GError                 **error)
{
  guint8 ddir[4096];
  GetFileData data = { 0 };
  gboolean success;
  GError *tmp_error = NULL;

  g_return_val_if_fail (filename != NULL, FALSE);

  data.filename = filename;
  data.time = time;
  data.is_dir = TRUE;
  data.found = FALSE;

  while (data.is_dir) {
    data.filename = filename;
    data.time = time;
    data.is_dir = FALSE;
    data.found = FALSE;

    highscore_disc_file_info_foreach_file (file_info, 4096, get_file_co, &data);

    if (data.found && data.is_dir) {
      success = highscore_disc_image_read_directory (disc, time, ddir, cancellable, &tmp_error);
      if (tmp_error != NULL) {
        g_propagate_error (error, tmp_error);

        return FALSE;
      }

      if (!success)
        return FALSE;

      file_info = (HighscoreDiscFileInfo *) ddir;

      break; // Parse the sub directory.
    }
  }

  return data.found;
}
