// This file is part of Highscore. License: GPL-3.0+.

#include "disc-image-time.h"

/* Private */

#define HIGHSCORE_DISC_IMAGE_FRAMES_PER_SECOND 75

static void
highscore_disc_image_time_get_minute_second_frame (const HighscoreDiscImageTime *time,
                                                   guint8                       *minute,
                                                   guint8                       *second,
                                                   guint8                       *frame)
{
  *minute = time->minute;
  *second = time->second;
  *frame = time->frame;
}

/* Public */

void
highscore_disc_image_time_set_minute_second_frame (HighscoreDiscImageTime *time,
                                                   guint8                  minute,
                                                   guint8                  second,
                                                   guint8                  frame)
{
  time->minute = minute;
  time->second = second;
  time->frame = frame;
}

void
highscore_disc_image_time_set_from_time_reference (HighscoreDiscImageTime *time,
                                                   guint8                 *time_reference)
{
  gint32 block; // The value of the clock containing the target time
  int minute, second, frame;

  block = GINT32_FROM_LE (*((gint32 *) time_reference));

  block += 2 * HIGHSCORE_DISC_IMAGE_FRAMES_PER_SECOND;
  minute = block / (60 * HIGHSCORE_DISC_IMAGE_FRAMES_PER_SECOND);
  block = block - minute * (60 * HIGHSCORE_DISC_IMAGE_FRAMES_PER_SECOND);
  second = block / HIGHSCORE_DISC_IMAGE_FRAMES_PER_SECOND;
  frame = block - second * HIGHSCORE_DISC_IMAGE_FRAMES_PER_SECOND;

  highscore_disc_image_time_set_minute_second_frame (time, minute, second, frame);
}

gint
highscore_disc_image_time_get_sector (const HighscoreDiscImageTime *time)
{
  guint8 minute, second, frame;
  highscore_disc_image_time_get_minute_second_frame (time, &minute, &second, &frame);

  return (minute * 60 + second - 2) * HIGHSCORE_DISC_IMAGE_FRAMES_PER_SECOND + frame;
}

void
highscore_disc_image_time_increment (HighscoreDiscImageTime *time)
{
  guint8 minute, second, frame;
  highscore_disc_image_time_get_minute_second_frame (time, &minute, &second, &frame);

  frame++;
  if (frame == HIGHSCORE_DISC_IMAGE_FRAMES_PER_SECOND) {
    frame = 0;
    second++;
    if (second == 60) {
      second = 0;
      minute++;
    }
  }

  highscore_disc_image_time_set_minute_second_frame (time, minute, second, frame);
}
