// This file is part of Highscore. License: GPL-3.0+.

#ifndef DISC_FILE_INFO_H
#define DISC_FILE_INFO_H

#include <glib.h>

G_BEGIN_DECLS

typedef struct _HighscoreDiscFileInfo HighscoreDiscFileInfo;

struct _HighscoreDiscFileInfo {
  guint8 length;
  guint8 ext_attr_length;
  guint8 extent[8];
  guint8 size[8];
  guint8 date[7];
  guint8 flags;
  guint8 file_unit_size;
  guint8 interleave;
  guint8 volume_sequence_number[4];
  guint8 name_length;
};

typedef gboolean (*HighscoreDiscFileInfoForeachCallback) (HighscoreDiscFileInfo *file_info, gpointer user_data);

gboolean highscore_disc_file_info_is_directory (HighscoreDiscFileInfo *file_info);
gchar *highscore_disc_file_info_access_name (HighscoreDiscFileInfo *file_info);
void highscore_disc_file_info_foreach_file (HighscoreDiscFileInfo                *file_info,
                                            gsize                                 size,
                                            HighscoreDiscFileInfoForeachCallback  callback,
                                            gpointer                              user_data);

G_END_DECLS

#endif /* DISC_FILE_INFO_H */
