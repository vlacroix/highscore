// This file is part of Highscore. License: GPL-3.0+.

errordomain Highscore.SegaSaturnError {
	INVALID_CUE_SHEET,
	INVALID_FILE_TYPE,
	CANT_READ_FILE,
	INVALID_HEADER,
}
