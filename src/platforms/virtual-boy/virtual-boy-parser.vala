// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.VirtualBoyParser : GameParser {
	public VirtualBoyParser (Platform platform, Uri uri) {
		base (platform, uri);
	}

	public override void parse () throws Error {
		var file = uri.to_file ();

		var header = new VirtualBoyHeader (file);
		header.check_validity ();
	}
}
