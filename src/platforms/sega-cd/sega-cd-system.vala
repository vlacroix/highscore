// This file is part of Highscore. License: GPL-3.0+.

private enum Highscore.SegaCDSystem {
	INVALID,
	SEGA_CD,
	SEGA_CD_32X,
}
