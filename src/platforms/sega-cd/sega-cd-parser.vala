// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.SegaCDParser : GameParser {
	private const string SEGA_CD_PLATFORM_ID = "SegaCD";
	private const string SEGA_CD_32X_PLATFORM_ID = "SegaCD32X";
	private const string CUE_MIME_TYPE = "application/x-cue";
	private const string SEGA_CD_MIME_TYPE = "application/x-sega-cd-rom";

	private string uid;

	public SegaCDParser (Platform platform, Uri uri) {
		base (platform, uri);
	}

	public override void parse () throws Error {
		var file = uri.to_file ();
		var file_info = file.query_info (FileAttribute.STANDARD_CONTENT_TYPE, FileQueryInfoFlags.NONE);
		var mime_type = file_info.get_content_type ();

		File bin_file;
		switch (mime_type) {
		case CUE_MIME_TYPE:
			var cue = new CueSheet (file);
			bin_file = get_binary_file (cue);

			break;
		case SEGA_CD_MIME_TYPE:
			bin_file = file;

			break;
		default:
			throw new SegaCDError.INVALID_FILE_TYPE ("Invalid file type: expected %s or %s but got %s for file %s.", CUE_MIME_TYPE, SEGA_CD_MIME_TYPE, mime_type, uri.to_string ());
		}

		var header = new SegaCDHeader (bin_file);
		header.check_validity ();

		if (platform.get_id () == SEGA_CD_PLATFORM_ID && !header.is_sega_cd ())
			throw new SegaCDError.INVALID_PLATFORM ("Invalid platform: expected %s\n", platform.get_id ());

		if (platform.get_id () == SEGA_CD_32X_PLATFORM_ID && !header.is_sega_cd_32x ())
			throw new SegaCDError.INVALID_PLATFORM ("Invalid platform: expected %s\n", platform.get_id ());

		var bin_uri = new Uri (bin_file.get_uri ());
		var header_offset = header.get_offset ();
		uid = Fingerprint.get_uid_for_chunk (bin_uri, platform.get_uid_prefix (), header_offset, SegaCDHeader.HEADER_LENGTH);
	}

	public override string get_uid () {
		return uid;
	}

	private static File get_binary_file (CueSheet cue) throws Error {
		if (cue.tracks_number == 0)
			throw new SegaCDError.INVALID_CUE_SHEET ("The file “%s” doesn’t have a track.", cue.file.get_uri ());

		var track = cue.get_track (0);
		var file = track.file;

		if (file.file_format != CueSheetFileFormat.BINARY && file.file_format != CueSheetFileFormat.UNKNOWN)
			throw new SegaCDError.INVALID_CUE_SHEET ("The file “%s” doesn’t have a valid binary file format.", cue.file.get_uri ());

		if (!track.track_mode.is_mode1 ())
			throw new SegaCDError.INVALID_CUE_SHEET ("The file “%s” doesn’t have a valid track mode for track %d.", cue.file.get_uri (), track.track_number);

		var header = new SegaCDHeader (file.file);
		header.check_validity ();

		return file.file;
	}
}
