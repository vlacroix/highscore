// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.DreamcastParser : GameParser {
	private const string GDI_MIME_TYPE = "application/x-gd-rom-cue";
	private const string CDI_MIME_TYPE = "application/x-discjuggler-cd-image";

	private string uid;

	public DreamcastParser (Platform platform, Uri uri) {
		base (platform, uri);
	}

	private File get_binary_file (Gdi gdi) throws Error {
		if (gdi.tracks_number == 0)
			throw new DreamcastError.INVALID_GDI ("The file “%s” doesn’t have a track.", gdi.file.get_uri ());

		var track = gdi.get_track (0);
		var file = track.file;

		var file_info = file.query_info ("*", FileQueryInfoFlags.NONE);
		if (file_info.get_content_type () != platform.get_presentation_mime_type ())
			throw new DreamcastError.INVALID_FILE_TYPE ("The file “%s” doesn’t have a valid Dreamcast binary file.", gdi.file.get_uri ());

		return file;
	}

	public override void parse () throws Error {
		var file = uri.to_file ();
		var file_info = file.query_info (FileAttribute.STANDARD_CONTENT_TYPE, FileQueryInfoFlags.NONE);
		var mime_type = file_info.get_content_type ();

		File bin_file;
		switch (mime_type) {
		case GDI_MIME_TYPE:
			var gdi = new Gdi (file);
			gdi.parse ();
			bin_file = get_binary_file (gdi);

			break;
		case CDI_MIME_TYPE:
			bin_file = file;

			break;
		default:
			throw new DreamcastError.INVALID_FILE_TYPE ("Invalid file type: expected %s or %s but got %s for file %s.", GDI_MIME_TYPE, CDI_MIME_TYPE, mime_type, uri.to_string ());
		}

		var header = new DreamcastHeader (bin_file);
		header.check_validity ();

		var prefix = platform.get_uid_prefix ();
		var product_number = header.get_product_number ();
		var areas = header.get_areas ();

		uid = @"$prefix-$product_number-$areas".down ();
	}

	public override string get_uid () {
		return uid;
	}
}
