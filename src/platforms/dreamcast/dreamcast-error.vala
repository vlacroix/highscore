// This file is part of Highscore. License: GPL-3.0+.

errordomain Highscore.DreamcastError {
	INVALID_GDI,
	INVALID_FILE_TYPE,
	INVALID_HEADER,
}
