// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Platform : Object {
	private string name;
	private string id;
	private string[] mime_types;
	private string presentation_mime_type;
	private string prefix;

	public bool autodiscovery { get; set; default = true; }
	public Type snapshot_type { get; set; default = typeof (Snapshot); }
	public Type runner_type { get; set; default = typeof (Runner); }
	public Type parser_type { get; set; default = typeof (GameParser); }

	public Platform (string id, string name,  string mime_type, string prefix) {
		this.id = id;
		this.name = name;
		this.mime_types = { mime_type };
		this.presentation_mime_type = mime_type;
		this.prefix = prefix;
	}

	public Platform.with_mime_types (string id, string name, string[] mime_types, string presentation_mime_type, string prefix) {
		this.id = id;
		this.name = name;
		this.mime_types = mime_types;
		this.presentation_mime_type = presentation_mime_type;
		this.prefix = prefix;
	}

	public string get_id () {
		return id;
	}

	public string get_name () {
		return name;
	}

	public string get_uid_prefix () {
		return prefix;
	}

	public string[] get_mime_types () {
		return mime_types;
	}

	public string get_presentation_mime_type () {
		return presentation_mime_type;
	}

	public static uint hash (Platform platform) {
		return str_hash (platform.get_id ());
	}

	public static bool equal (Platform a, Platform b) {
		return a == b || str_equal (a.get_id (), b.get_id ());
	}

	public static int compare (Platform a, Platform b) {
		return a.get_name ().collate (b.get_name ());
	}

	public string get_system_dir () {
		var platforms_dir = Application.get_platforms_dir ();
		var id = get_id ();

		return @"$platforms_dir/$id/system";
	}
}
