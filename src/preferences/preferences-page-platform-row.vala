// This file is part of Highscore. License: GPL-3.0+.

private class Highscore.PreferencesPagePlatformRow : Adw.ComboRow {
	public Platform platform { get; construct; }

	public PreferencesPagePlatformRow (Platform platform) {
		Object (platform: platform);
	}

	construct {
		title = platform.get_name ();

		use_subtitle = true;

		var store = new ListStore (typeof (Retro.CoreDescriptor));

		var core_manager = CoreManager.get_instance ();
		var cores = core_manager.get_cores_for_platform (platform);

		foreach (var core in cores)
			store.append (core);

		model = store;

		expression = new Gtk.CClosureExpression (typeof (string),
		                                         null, {},
		                                         (Callback) get_core_name,
		                                         null, null);

		notify["selected"].connect (notify_selected_cb);

		if (store.get_n_items () < 1) {
			use_subtitle = false;

			/* Translators: This is displayed under the platform name when no
			 * core is available for this platform. To see this message, click
			 * on the hamburger menu, click on Preferences, then on Platforms */
			subtitle = _("None");
		}
	}

	private static string get_core_name (Retro.CoreDescriptor core) {
		try {
			return core.get_name ();
		}
		catch (Error e) {
			return core.get_id ();
		}
	}

	private void notify_selected_cb () {
		var core = model.get_item (selected) as Retro.CoreDescriptor;

		if (core == null)
			return;

		var core_manager = CoreManager.get_instance ();
		core_manager.set_preferred_core (platform, core);
	}
}
