// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/preferences/preferences-page-controllers.ui")]
private class Highscore.PreferencesPageControllers : Adw.PreferencesPage {
	[GtkChild]
	private unowned Adw.PreferencesGroup gamepads_group;
	[GtkChild]
	private unowned Adw.PreferencesGroup keyboard_group;

	private Manette.Monitor monitor;

	private Adw.ActionRow[] gamepad_rows;

	construct {
		monitor = new Manette.Monitor ();
		monitor.device_connected.connect (rebuild_gamepad_list);
		monitor.device_disconnected.connect (rebuild_gamepad_list);
		gamepad_rows = {};
		build_gamepad_list ();
		build_keyboard_list ();
	}

	private void rebuild_gamepad_list () {
		clear_gamepad_list ();
		build_gamepad_list ();
	}

	private void build_gamepad_list () {
		Manette.Device device = null;
		var i = 0;
		var iterator = monitor.iterate ();

		while (iterator.next (out device)) {
			var row = new Adw.ActionRow ();
			row.title = device.get_name ();
			row.add_suffix (new Gtk.Image.from_icon_name ("go-next-symbolic"));
			row.activatable = true;

			// The original device variable will be overwritten on the
			// next iteration, while this one will remain there and can
			// be used in the event handler.
			var this_device = device;

			row.activated.connect (() => {
				var window = get_root () as PreferencesWindow;
				window.open_subpage (new PreferencesSubpageGamepad (this_device));
			});

			gamepads_group.add (row);
			gamepad_rows += row;

			i += 1;
		}

		gamepads_group.visible = i > 0;
	}

	private void clear_gamepad_list () {
		foreach (var row in gamepad_rows)
			gamepads_group.remove (row);

		gamepad_rows = {};
	}

	private void build_keyboard_list () {
		var row = new Adw.ActionRow ();
		row.title = _("Keyboard");
		row.add_suffix (new Gtk.Image.from_icon_name ("go-next-symbolic"));
		row.activatable = true;

		row.activated.connect (() => {
			var window = get_root () as PreferencesWindow;
			window.open_subpage (new PreferencesSubpageKeyboard ());
		});

		keyboard_group.add (row);
	}
}
