// This file is part of Highscore. License: GPL-3.0+.

private interface Highscore.PreferencesSubpage : Gtk.Widget {
	public signal void back ();

	public abstract bool allow_back { get; set; }
}
