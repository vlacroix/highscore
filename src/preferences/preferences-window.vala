// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/preferences/preferences-window.ui")]
private class Highscore.PreferencesWindow : Adw.PreferencesWindow {
	private Binding swipe_back_binding;

	public void open_subpage (PreferencesSubpage subpage) {
		swipe_back_binding = subpage.bind_property (
			"allow-back", this, "can-navigate-back", BindingFlags.SYNC_CREATE);

		subpage.back.connect (() => {
			swipe_back_binding.unbind ();
			close_subpage ();
		});

		present_subpage (subpage);
	}
}
