// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/preferences/preferences-page-video.ui")]
private class Highscore.PreferencesPageVideo : Adw.PreferencesPage {
	private string _filter_active;
	public string filter_active {
		set {
			for (var i = 0; i < filter_names.length; i++) {
				filter_radios[i].active = (value == filter_names[i]);
			}
			_filter_active = value;
		}

		get {
			return _filter_active;
		}
	}

	[GtkChild]
	private unowned Gtk.Switch hd_switch;
	[GtkChild]
	private unowned Adw.PreferencesGroup filter_group;

	// same as video-filters in gschema
	/* Translators: These values are video filters applied to the screen. Smooth
	* tries to smoothen the pixels, sharp displays the pixels square, and CRT
	* emulates an old TV */
	private string[] filter_display_names = { _("Smooth"), _("Sharp"), _("CRT") };
	private string[] filter_names = { "smooth", "sharp", "crt" };

	private Settings settings;
	private Gtk.CheckButton filter_radios[3];

	construct {
		for (var i = 0; i < filter_display_names.length; i++) {
			var row = new Adw.ActionRow ();
			row.title = filter_display_names [i];

			filter_radios[i] = new Gtk.CheckButton ();

			if (i > 0)
				filter_radios[i].group = filter_radios[0];

			filter_radios[i].name = filter_names[i];
			filter_radios[i].valign = Gtk.Align.CENTER;
			filter_radios[i].toggled.connect ((radio) => {
				if (!radio.active)
					return;

				filter_active = radio.name;
			});

			row.add_prefix (filter_radios[i]);
			row.activatable_widget = filter_radios[i];

			filter_group.add (row);
		}

		settings = new Settings ("org.gnome.World.Highscore");
		settings.bind ("hd", hd_switch, "active",
		               SettingsBindFlags.DEFAULT);
		settings.bind ("video-filter", this, "filter-active",
		               SettingsBindFlags.DEFAULT);
	}
}
