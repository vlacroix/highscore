// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/preferences/preferences-page-platforms.ui")]
private class Highscore.PreferencesPagePlatforms : Adw.PreferencesPage {
	[GtkChild]
	private unowned Adw.PreferencesGroup platforms_group;

	construct {
		var register = PlatformRegister.get_register ();
		var platforms = register.get_all_platforms ();

		foreach (var platform in platforms) {
			var row = new PreferencesPagePlatformRow (platform);

			platforms_group.add (row);
		}
	}
}
