// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.CompositeCover : Object, Cover {
	private Cover[] covers;

	public CompositeCover (Cover[] covers) {
		this.covers = covers;
		foreach (var cover in covers)
			if (cover != null)
				cover.changed.connect (on_cover_changed);
	}

	private Cover lookup_cover () {
		foreach (var cover in covers)
			if (cover != null && cover.get_cover () != null)
				return cover;

		return new DummyCover ();
	}

	public GLib.File? get_cover () {
		return lookup_cover ().get_cover ();
	}

	public Gdk.RGBA get_background_color () {
		return lookup_cover ().get_background_color ();
	}

	private void on_cover_changed () {
		changed ();
	}
}
