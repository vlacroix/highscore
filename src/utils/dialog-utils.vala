// This file is part of Highscore. License: GPL-3.0+.

namespace Highscore.DialogUtils {
	public async Gtk.ResponseType run_async (Gtk.Dialog dialog) {
		var response = Gtk.ResponseType.CANCEL;

		dialog.response.connect (r => {
			response = (Gtk.ResponseType) r;

			run_async.callback ();
		});

		dialog.present ();

		yield;

		return response;
	}

	public async string run_message_async (Adw.MessageDialog dialog) {
		string response = dialog.close_response;

		dialog.response.connect (r => {
			response = r;

			run_message_async.callback ();
		});

		dialog.present ();

		yield;

		return response;
	}

	public async Gtk.ResponseType run_native_async (Gtk.NativeDialog dialog) {
		var response = Gtk.ResponseType.CANCEL;

		dialog.response.connect (r => {
			response = (Gtk.ResponseType) r;

			run_native_async.callback ();
		});

		dialog.show ();

		yield;

		return response;
	}
}
