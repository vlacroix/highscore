// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Filename : Object {
	private static Regex filename_ext_regex = null;

	public static string get_title (Uri uri) {
		var file = uri.to_file ();
		string name;
		try {
			var fileinfo = file.query_info (FileAttribute.STANDARD_DISPLAY_NAME,
			                                FileQueryInfoFlags.NONE,
			                                null);
			name = fileinfo.get_display_name ();
		} catch (Error e) {
			critical ("Couldn't retrieve filename: %s", e.message);
			name = file.get_basename ();
		}

		try {
			if (filename_ext_regex == null)
				filename_ext_regex = /\.\w+$/;;

			name = filename_ext_regex.replace (name, name.length, 0, "");
		} catch (RegexError e) {
			error ("Regex replacement failed: %s", e.message);
		}

		name = name.split ("(")[0];
		name = name.strip ();

		return name;
	}
}
