// This file is part of Highscore. License: GPL-3.0+.

private errordomain Highscore.UriError {
	INVALID_SCHEME,
}
