// This file is part of Highscore. License: GPL-3.0+.

int main (string[] args) {
	Intl.bindtextdomain (Config.GETTEXT_PACKAGE, Config.GNOMELOCALEDIR);
	Intl.bind_textdomain_codeset (Config.GETTEXT_PACKAGE, "UTF-8");
	Intl.textdomain (Config.GETTEXT_PACKAGE);

	// Needed for shortcuts window
	typeof (ThemedIcon).ensure ();

	var app = new Highscore.Application ();
	var result = app.run (args);

	return result;
}
