// This file is part of Highscore. License: GPL-3.0+.

private struct Highscore.GamepadDPad {
	int32 axis_values[2];
}
