// This file is part of Highscore. License: GPL-3.0+.

private struct Highscore.GamepadInput {
	uint16 type;
	uint16 code;
}
