// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.DummyCover : Object, Cover {
	public File? get_cover () {
		return null;
	}

	public Gdk.RGBA get_background_color () {
		return { 0.0f, 0.0f, 0.0f, 1.0f };
	}
}
