// This file is part of Highscore. License: GPL-3.0+.

namespace Highscore.Credits {
	private const string[] DEVELOPERS = {
		"Abhinav Singh <theawless@gmail.com>",
		"Adrien Plazas <kekun.plazas@laposte.net>",
		"Alexander Mikhaylenko <alexm@gnome.org>",
		"Andrei Lişiţă <andreii.lisita@gmail.com>",
		"Megh Parikh <meghprkh@gmail.com>",
		"Neville Antony <nevilleantony98@gmail.com>",
		"Ricard Gascons <gascons1995@gmail.com>",
		"Saurabh Singh <saurabhsingh412@gmail.com>",
		"Sebastien Nicouleaud <sebn@users.noreply.github.com>",
		null
	};

	private const string[] DESIGNERS = {
		"Adrien Plazas <kekun.plazas@laposte.net>",
		"Jakub Steiner <jimmac@gmail.com>",
		"Tobias Bernard <tbernard@gnome.org>",
		null
	};

	private const string[] DOCUMENTERS = {
		"Adrien Plazas <kekun.plazas@laposte.net>",
		"Laurent Pointecouteau <laurent.pointecouteau@gmail.com>",
		null
	};
}
