// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/display-view.ui")]
private class Highscore.DisplayView : Adw.Bin, UiView {
	private const uint FOCUS_OUT_DELAY_MILLISECONDS = 500;

	public signal void back ();

	[GtkChild]
	private unowned Gtk.Stack headerbar_stack;
	[GtkChild]
	private unowned Gtk.HeaderBar ingame_header_bar;
	[GtkChild]
	private unowned Gtk.Button fullscreen;
	[GtkChild]
	private unowned Gtk.Button restore;
	[GtkChild]
	private unowned Gtk.MenuButton secondary_menu_button;
	[GtkChild]
	private unowned Gtk.PopoverMenu secondary_menu_popover;
	[GtkChild]
	private unowned Gtk.HeaderBar snapshots_header_bar;
	[GtkChild]
	private unowned MediaMenuButton media_button;
	[GtkChild]
	private unowned InputModeSwitcher input_mode_switcher;
	[GtkChild]
	private unowned Gtk.Stack stack;
	[GtkChild]
	private unowned Adw.StatusPage error_display;
	[GtkChild]
	private unowned Gtk.Button restart_btn;
	[GtkChild]
	private unowned Adw.Flap display_flap;
	[GtkChild]
	private unowned Adw.Bin display_bin;
	[GtkChild]
	private unowned FullscreenBox fullscreen_box;
	[GtkChild]
	private unowned FlashBox flash_box;
	[GtkChild]
	private unowned SnapshotsList snapshots_list;

	private bool _is_view_active;
	public bool is_view_active {
		get { return _is_view_active; }
		set {
			if (is_view_active == value)
				return;

			_is_view_active = value;

			if (!is_view_active) {
				is_fullscreen = false;

				if (runner != null) {
					runner.stop ();
					runner = null;
				}

				update_actions ();
			}
		}
	}

	public Gtk.Window window { get; construct; }

	public bool can_fullscreen { get; set; }
	public bool is_fullscreen { get; set; }
	public bool is_showing_snapshots { get; set; }
	public bool is_menu_open { get; set; }
	public string game_title { get; set; }

	private Runner _runner;
	public Runner runner {
		get { return _runner; }
		set {
			if (runner != null)
				runner.snapshot_created.disconnect (flash_box.flash);

			_runner = value;
			display_bin.child = null;

			if (runner == null)
				return;

			stack.visible_child = display_flap;

			var display = runner.get_display ();
			set_display (display);

			snapshots_list.runner = value;
			input_mode_switcher.runner = value;

			if (runner != null)
				extra_widget = runner.get_extra_widget ();
			else
				extra_widget = null;

			secondary_menu_button.visible = runner != null;

			runner.snapshot_created.connect (flash_box.flash);
		}
	}

	private HeaderBarWidget _extra_widget;
	private HeaderBarWidget extra_widget {
		get { return _extra_widget; }
		set {
			if (extra_widget == value)
				return;

			if (extra_widget != null) {
				extra_widget.disconnect (extra_widget_notify_popover_visible_id);
				ingame_header_bar.remove (extra_widget);
				extra_widget_notify_popover_visible_id = 0;
			}

			_extra_widget = value;

			if (extra_widget != null) {
				extra_widget_notify_popover_visible_id = extra_widget.notify["popover-visible"].connect (update_fullscreen_box);
				ingame_header_bar.pack_end (extra_widget);
			}
		}
	}

	private Settings settings;

	private Cancellable run_game_cancellable;
	private Cancellable quit_game_cancellable;

	private Adw.MessageDialog resume_dialog;
	private Adw.MessageDialog resume_failed_dialog;
	private Adw.MessageDialog quit_dialog;
	private Adw.MessageDialog restart_dialog;

	private ulong extra_widget_notify_popover_visible_id;

	private Game game;

	construct {
		settings = new Settings ("org.gnome.World.Highscore");
	}

	static construct {
		install_action ("display.create-snapshot", null, widget => {
			var self = widget as DisplayView;

			self.create_new_snapshot ();
		});

		install_action ("display.load-snapshot", null, widget => {
			var self = widget as DisplayView;

			self.load_snapshot ();
		});

		install_action ("display.show-snapshots", null, widget => {
			var self = widget as DisplayView;

			self.show_snapshots ();
		});

		install_action ("display.restart", null, widget => {
			var self = widget as DisplayView;

			self.restart_internal.begin ();
		});

		install_property_action ("display.fullscreen", "is-fullscreen");

		add_binding_action (Gdk.Key.a, Gdk.ModifierType.CONTROL_MASK,
		                    "display.show-snapshots", null);
		add_binding_action (Gdk.Key.s, Gdk.ModifierType.CONTROL_MASK,
		                    "display.create-snapshot", null);
		add_binding_action (Gdk.Key.d, Gdk.ModifierType.CONTROL_MASK,
		                    "display.load-snapshot", null);
		add_binding_action (Gdk.Key.f, Gdk.ModifierType.CONTROL_MASK,
		                    "display.fullscreen", null);

		add_binding_action (Gdk.Key.F2, 0, "display.create-snapshot", null);
		add_binding_action (Gdk.Key.F3, 0, "display.load-snapshot", null);
		add_binding_action (Gdk.Key.F4, 0, "display.show-snapshots", null);
		add_binding_action (Gdk.Key.F11, 0, "display.fullscreen", null);

		add_binding (Gdk.Key.Left, Gdk.ModifierType.ALT_MASK, widget => {
			var self = widget as DisplayView;

			if (self.get_direction () == Gtk.TextDirection.RTL)
				return Gdk.EVENT_PROPAGATE;

			self.on_display_back ();

			return Gdk.EVENT_STOP;
		}, null);

		add_binding (Gdk.Key.Right, Gdk.ModifierType.ALT_MASK, widget => {
			var self = widget as DisplayView;

			if (self.get_direction () == Gtk.TextDirection.LTR)
				return Gdk.EVENT_PROPAGATE;

			self.on_display_back ();

			return Gdk.EVENT_STOP;
		}, null);

		add_binding (Gdk.Key.Escape, 0, widget => {
			var self = widget as DisplayView;

			if (self.is_showing_snapshots) {
				self.on_display_back ();

				return Gdk.EVENT_STOP;
			}

			if (self.is_fullscreen) {
				self.is_fullscreen = false;

				return Gdk.EVENT_STOP;
			}

			return Gdk.EVENT_PROPAGATE;
		}, null);
	}

	[GtkCallback]
	private void on_back_button_pressed () {
		back ();
	}

	private void create_new_snapshot () {
		runner.pause ();
		runner.try_create_snapshot (false);
		runner.resume ();
		runner.get_display ().grab_focus ();
	}

	public bool gamepad_button_press_event (Manette.Event event) {
		if (resume_dialog != null)
			return handle_dialog_gamepad_button_press_event (resume_dialog, event, "restart", "resume");

		if (resume_failed_dialog != null)
			return handle_dialog_gamepad_button_press_event (resume_failed_dialog, event, "quit", "reset");

		if (quit_dialog != null)
			return handle_dialog_gamepad_button_press_event (quit_dialog, event, "cancel", "quit");

		if (restart_dialog != null)
			return handle_dialog_gamepad_button_press_event (restart_dialog, event, "cancel", "restart");

		if (!window.is_active || !window.get_mapped ())
			return false;

		uint16 button;
		if (!event.get_button (out button))
			return false;

		if (!get_mapped ())
			return false;

		if (runner == null)
			return false;

		if (runner.gamepad_button_press_event (button))
			return true;

		switch (button) {
		case EventCode.BTN_MODE:
			on_display_back ();

			return true;
		default:
			return false;
		}
	}

	public bool gamepad_button_release_event (Manette.Event event) {
		return false;
	}

	public bool gamepad_absolute_axis_event (Manette.Event event) {
		return false;
	}

	private void on_display_back () {
		if (is_showing_snapshots) {
			runner.preview_current_state ();
			is_showing_snapshots = false;

			return;
		}

		back ();
	}

	public async void run_game (Game game) {
		// If there is a game already running we have to quit it first
		if (runner != null && !yield quit_game ())
			return;

		this.game = game;

		if (run_game_cancellable != null)
			run_game_cancellable.cancel ();

		var cancellable = new Cancellable ();
		run_game_cancellable = cancellable;

		yield run_game_with_cancellable (game, cancellable);

		// Only reset the cancellable if another one didn't replace it.
		if (run_game_cancellable == cancellable)
			run_game_cancellable = null;
	}

	private async void run_game_with_cancellable (Game game, Cancellable cancellable) {
		game_title = game.title;

		// Reset the UI parts depending on the runner to avoid an
		// inconsistent state is case we couldn't retrieve it.
		reset_display_page ();

		runner = try_get_runner (game);
		if (runner == null)
			return;

		can_fullscreen = runner.can_fullscreen;
		media_button.media_set = runner.media_set;

		runner.crash.connect (message => {
			runner.stop ();
			reset_display_page ();

			if (run_game_cancellable != null)
				run_game_cancellable.cancel ();

			if (quit_game_cancellable != null)
				quit_game_cancellable.cancel ();

			stack.visible_child = error_display;
			is_showing_snapshots = false;

			error_display.title = _("Oops! The game crashed unexpectedly");
			error_display.description = message;
			restart_btn.show ();
		});

		update_actions ();

		runner.notify["supports-snapshots"].connect (update_actions);

		is_fullscreen = settings.get_boolean ("fullscreen") && can_fullscreen;
		settings.bind ("fullscreen", this, "is-fullscreen", DEFAULT);

		if (!runner.can_resume) {
			start_or_resume (runner, false);
			return;
		}

		if (resume_dialog != null)
			return;

		bool should_resume = yield prompt_resume_with_cancellable (cancellable);

		if (!start_or_resume (runner, should_resume))
			yield prompt_resume_fail_with_cancellable (runner, cancellable);
	}

	private Runner? try_get_runner (Game game) {
		var collection = Application.get_default ().get_collection ();
		var runner = collection.create_runner (game);

		assert (runner != null);

		try {
			runner.prepare ();
		}
		catch (RunnerError e) {
			reset_display_page ();

			stack.visible_child = error_display;

			error_display.title = _("Oops! Unable to run the game");
			error_display.description = e.message;
			restart_btn.hide ();

			return null;
		}

		return runner;
	}

	private async bool prompt_resume_with_cancellable (Cancellable cancellable) {
		resume_dialog = new Adw.MessageDialog (
			window,
			_("Resume last game?"),
			null
		);

		resume_dialog.add_response ("restart", _("_Restart"));
		resume_dialog.add_response ("resume", _("Re_sume"));
		resume_dialog.default_response = "resume";
		resume_dialog.close_response = "resume";

		resume_dialog.set_response_appearance ("resume", SUGGESTED);

		cancellable.cancelled.connect (() => {
			resume_dialog.destroy ();
			resume_dialog = null;
		});

		var response = yield DialogUtils.run_message_async (resume_dialog);

		resume_dialog = null;

		return response == "resume";
	}

	private bool start_or_resume (Runner runner, bool resume) {
		try {
			if (resume)
				runner.load_previewed_snapshot ();

			runner.start ();

			return true;
		}
		catch (Error e) {
			warning (e.message);

			return false;
		}
	}

	private async void prompt_resume_fail_with_cancellable (Runner runner, Cancellable cancellable) {
		if (resume_failed_dialog != null)
			return;

		resume_failed_dialog = new Adw.MessageDialog (
			window,
			_("Resuming failed"),
			_("Do you want to restart the game?")
		);

		resume_failed_dialog.add_response ("quit", _("_Quit"));
		resume_failed_dialog.add_response ("reset", C_("Resuming a game failed dialog", "_Reset"));
		resume_failed_dialog.default_response = "quit";
		resume_failed_dialog.close_response = "quit";

		resume_failed_dialog.set_response_appearance ("reset", DESTRUCTIVE);

		cancellable.cancelled.connect (() => {
			resume_failed_dialog.destroy ();
			resume_failed_dialog = null;
		});

		var response = yield DialogUtils.run_message_async (resume_failed_dialog);

		resume_failed_dialog = null;

		if (cancellable.is_cancelled ())
			response = "quit";

		if (response == "quit") {
			runner = null;
			back ();

			return;
		}

		try {
			runner.start ();
		}
		catch (Error e) {
			warning (e.message);
		}
	}

	public async bool quit_game () {
		if (run_game_cancellable != null)
			run_game_cancellable.cancel ();

		if (quit_game_cancellable != null)
			quit_game_cancellable.cancel ();

		var cancellable = new Cancellable ();
		quit_game_cancellable = cancellable;

		var result = yield quit_game_with_cancellable (cancellable);

		// Only reset the cancellable if another one didn't replace it.
		if (quit_game_cancellable == cancellable)
			quit_game_cancellable = null;

		return result;
	}

	private async bool quit_game_with_cancellable (Cancellable cancellable) {
		if (runner == null)
			return true;

		runner.pause ();

		if (runner.try_create_snapshot (true) != null) {
			// Progress saved => can quit game safely
			runner.stop ();
			return true;
		}

		// Failed to save progress => warn the user of unsaved progress
		// via the QuitDialog
		if (quit_dialog != null)
			return false;

		quit_dialog = new Adw.MessageDialog (
			window,
			_("Are you sure you want to quit?"),
			_("All unsaved progress will be lost.")
		);

		quit_dialog.add_response ("cancel", _("_Cancel"));
		quit_dialog.add_response ("quit", _("_Quit"));
		quit_dialog.default_response = "cancel";
		quit_dialog.close_response = "cancel";
		quit_dialog.set_response_appearance ("quit", DESTRUCTIVE);

		cancellable.cancelled.connect (() => {
			quit_dialog.destroy ();
			quit_dialog = null;
		});

		var response = yield DialogUtils.run_message_async (quit_dialog);

		quit_dialog = null;

		if (cancellable.is_cancelled ())
			return cancel_quitting_game ();

		if (response == "quit")
			return true;

		return cancel_quitting_game ();
	}

	private bool cancel_quitting_game () {
		if (runner != null)
			runner.resume ();

		return false;
	}

	private void reset_display_page () {
		Settings.unbind (this, "is-fullscreen");
		can_fullscreen = false;
		is_fullscreen = false;
		runner = null;
		media_button.media_set = null;
		secondary_menu_button.visible = false;
		extra_widget = null;

		update_actions ();
	}

	[GtkCallback]
	private void maybe_resume_game () {
		if (window.is_active && display_flap.reveal_progress <= 0 && !display_flap.reveal_flap) {
			runner.resume ();
			runner.get_display ().grab_focus ();
		}
	}

	public void update_pause () {
		if (!can_update_pause ())
			return;

		if (window.is_active) {
			if (!is_showing_snapshots) {
				runner.resume ();
				runner.get_display ().grab_focus ();
			}
		}
		else
			runner.pause ();
	}

	private bool can_update_pause () {
		if (runner == null)
			return false;

		if (run_game_cancellable != null)
			return false;

		if (quit_game_cancellable != null)
			return false;

		if (restart_dialog != null)
			return false;

		return true;
	}

	[GtkCallback]
	private void update_actions () {
		bool supports_snapshots = runner != null && runner.supports_snapshots;
		action_set_enabled ("view.show-snapshots", supports_snapshots);
		action_set_enabled ("display.load_snapshot",
		                    supports_snapshots && (snapshots_list.has_snapshot_selected || !is_showing_snapshots));
	}

	private void load_snapshot () {
		if (!is_showing_snapshots) {
			var snapshots = runner.get_snapshots ();

			if (snapshots.length == 0)
				return;

			runner.pause ();
			runner.preview_snapshot (snapshots[0]);
		}

		try {
			runner.load_previewed_snapshot ();
		}
		catch (Error e) {
			warning ("Failed to load snapshot: %s", e.message);
		}

		// If we're showing snapshots, the game will resume
		// on its own after the hide animation ends
		if (!is_showing_snapshots) {
			runner.resume ();
			runner.get_display ().grab_focus ();
		}

		is_showing_snapshots = false;
	}

	private void show_snapshots () {
		if (runner != null) {
			is_showing_snapshots = true;
			snapshots_list.reveal ();
		}
	}

	private async void restart_internal () {
		if (runner == null) {
			yield run_game (game);

			return;
		}

		runner.pause ();

		if (runner.try_create_snapshot (true) == null) {
			restart_dialog = new Adw.MessageDialog (
				window,
				_("Are you sure you want to restart?"),
				_("All unsaved progress will be lost.")
			);

			restart_dialog.add_response ("cancel", _("_Cancel"));
			restart_dialog.add_response ("restart", _("_Restart"));
			restart_dialog.default_response = "cancel";
			restart_dialog.close_response = "cancel";
			restart_dialog.set_response_appearance ("restart", DESTRUCTIVE);

			var response = yield DialogUtils.run_message_async (restart_dialog);

			restart_dialog = null;

			if (response == "cancel") {
				runner.resume ();

				return;
			}
		}

		runner.stop ();

		runner = try_get_runner (game);

		runner.notify["supports-snapshots"].connect (update_actions);

		try {
			runner.start ();
		}
		catch (Error e) {
			critical ("Couldn't restart: %s", e.message);
		}
	}

	private void set_display (Gtk.Widget display) {
		display_bin.child = display;
	}

	[GtkCallback]
	private void update_fullscreen_box () {
		var is_menu_open = media_button.popover_visible ||
		                   secondary_menu_popover.visible ||
		                   (extra_widget != null && extra_widget.popover_visible);

		fullscreen_box.autohide = !is_menu_open &&
		                          !is_showing_snapshots;
		fullscreen_box.overlay = is_fullscreen && !is_showing_snapshots;
	}

	[GtkCallback]
	private void on_fullscreen_changed () {
		fullscreen.visible = can_fullscreen && !is_fullscreen;
		restore.visible = can_fullscreen && is_fullscreen;

		update_fullscreen_box ();
	}

	[GtkCallback]
	private void on_showing_snapshots_changed () {
		update_fullscreen_box ();

		if (is_showing_snapshots)
			headerbar_stack.visible_child = snapshots_header_bar;
		else
			headerbar_stack.visible_child = ingame_header_bar;
	}

	[GtkCallback]
	private void on_back_clicked () {
		on_display_back ();
	}

	[GtkCallback]
	private void on_fullscreen_clicked () {
		is_fullscreen = true;
	}

	[GtkCallback]
	private void on_restore_clicked () {
		is_fullscreen = false;
	}

	private bool handle_dialog_gamepad_button_press_event (Adw.MessageDialog dialog, Manette.Event event, string negative_response, string positive_response) {
		if (!visible)
			return false;

		if (!dialog.is_active)
			return false;

		uint16 button;
		if (!event.get_button (out button))
			return false;

		switch (button) {
		case EventCode.BTN_A:
			dialog.response (positive_response);

			return true;
		case EventCode.BTN_B:
			dialog.response (negative_response);

			return true;
		default:
			return false;
		}
	}
}
