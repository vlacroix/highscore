// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/error-info-bar.ui")]
private class Highscore.ErrorInfoBar : Adw.Bin {
	[GtkChild]
	private unowned Gtk.InfoBar info_bar;
	[GtkChild]
	private unowned Gtk.Label label;

	[GtkCallback]
	private void on_response () {
		info_bar.revealed = false;
	}

	public void show_error (string message) {
		label.label = message;
		info_bar.revealed = true;
	}
}
