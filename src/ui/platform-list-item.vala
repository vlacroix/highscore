// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/platform-list-item.ui")]
private class Highscore.PlatformListItem : Gtk.ListBoxRow {
	[GtkChild]
	protected unowned Gtk.Label label;

	public Platform platform { get; construct; }

	construct {
		label.label = platform.get_name ();
	}

	public PlatformListItem (Platform platform) {
		Object (platform: platform);
	}
}
