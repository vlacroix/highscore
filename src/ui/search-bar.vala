// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/search-bar.ui")]
private class Highscore.SearchBar : Adw.Bin {
	public string text { get; private set; }
	public bool search_mode_enabled { get; set; }
	public unowned Gtk.Widget key_capture_widget { get; set; }

	[GtkChild]
	private unowned Gtk.SearchBar search_bar;
	[GtkChild]
	private unowned Gtk.SearchEntry entry;

	construct {
		search_bar.connect_entry (entry);
	}

	[GtkCallback]
	private void on_search_changed () {
		text = entry.text;
	}

	[GtkCallback]
	private void on_search_activated () {
		text = entry.text;
	}

	public void focus_entry () {
		entry.grab_focus ();
	}

	public void run_search (string query) {
		entry.text = query;
	}
}
