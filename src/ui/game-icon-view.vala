// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/game-icon-view.ui")]
private class Highscore.GameIconView : Gtk.FlowBoxChild {
	public signal void secondary_click ();

	[GtkChild]
	private unowned GameThumbnail thumbnail;
	[GtkChild]
	private unowned Gtk.Label title;

	private ulong game_replaced_id;

	private Game _game;
	public Game game {
		get { return _game; }
		construct set {
			if (game == value)
				return;

			if (game_replaced_id > 0)
				game.disconnect (game_replaced_id);

			_game = value;

			thumbnail.game = game;
			title.label = game.title;

			game.bind_property ("is-favorite", this, "is-favorite", BindingFlags.SYNC_CREATE);
			game_replaced_id = game.replaced.connect (game_replaced);
		}
	}

	public bool checked { get; set; }
	public bool is_selection_mode { get; set; }
	public bool is_favorite { get; set; }

	construct {
		var click_gesture = new Gtk.GestureClick ();
		click_gesture.button = 0;
		click_gesture.pressed.connect (() => {
			var event = click_gesture.get_current_event ();

			if (event.triggers_context_menu ()) {
				secondary_click ();
				click_gesture.set_state (Gtk.EventSequenceState.CLAIMED);
			} else {
				click_gesture.set_state (Gtk.EventSequenceState.DENIED);
			}
		});
		add_controller (click_gesture);

		var long_press_gesture = new Gtk.GestureLongPress ();
		long_press_gesture.pressed.connect (() => {
			secondary_click ();
			long_press_gesture.set_state (Gtk.EventSequenceState.CLAIMED);
		});
		add_controller (long_press_gesture);
	}

	public GameIconView (Game game) {
		Object (game: game);
	}

	private void game_replaced (Game new_game) {
		game = new_game;
	}

	public static uint hash (GameIconView key) {
		return Game.hash (key.game);
	}

	public static bool equal (GameIconView a, GameIconView b) {
		return Game.equal (a.game, b.game);
	}
}
