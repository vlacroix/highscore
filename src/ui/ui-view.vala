// This file is part of Highscore. License: GPL-3.0+.

private interface Highscore.UiView : Gtk.Widget {
	public abstract bool is_view_active { get; set; }

	public abstract bool gamepad_button_press_event (Manette.Event event);
	public abstract bool gamepad_button_release_event (Manette.Event event);
	public abstract bool gamepad_absolute_axis_event (Manette.Event event);
}
