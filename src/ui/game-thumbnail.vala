// This file is part of Highscore. License: GPL-3.0+.

private class Highscore.GameThumbnail : Gtk.Widget {
	private const double EMBLEM_SCALE = 0.125;
	private const double EMBLEM_MIN_SIZE = 16;

	private ulong cover_changed_id;
	private Cover cover;

	private Game _game;
	public Game game {
		get { return _game; }
		set {
			if (_game == value)
				return;

			if (cover != null)
				cover.disconnect (cover_changed_id);

			_game = value;
			cover = game.get_cover ();

			try_load_cover = true;

			if (cover != null)
				cover_changed_id = cover.changed.connect (() => {
					try_load_cover = true;
					queue_draw ();
				});

			queue_draw ();
		}
	}

	private Gdk.Paintable? cover_paintable;
	private bool try_load_cover;
	private int last_scale_factor;
	private int last_cover_size;

	public struct DrawingContext {
		Gtk.Snapshot snapshot;
		int width;
		int height;
	}

	construct {
		overflow = HIDDEN;

		try_load_cover = true;
	}

	static construct {
		set_css_name ("gamesgamethumbnail");
	}

	protected override void snapshot (Gtk.Snapshot snapshot) {
		if (cover == null)
			return;

		DrawingContext context = {
			snapshot, get_width (), get_height ()
		};

		draw_image (context);
	}

	private void update_style_classes () {
		if (cover_paintable != null)
			add_css_class ("cover");
		else
			remove_css_class ("cover");
	}

	public void draw_image (DrawingContext context) {
		Gdk.Paintable cover;
		get_cover (context, out cover);

		if (cover != null) {
			draw_paintable (context, cover);

			return;
		}

		var emblem = get_emblem (context);

		if (emblem != null)
			draw_paintable (context, emblem);
	}

	private Gdk.Paintable? get_emblem (DrawingContext context) {
		var display = Gdk.Display.get_default ();
		var theme = Gtk.IconTheme.get_for_display (display);
		var size = int.min (context.width, context.height) * EMBLEM_SCALE * scale_factor;
		size = double.max (size, EMBLEM_MIN_SIZE * scale_factor);

		return theme.lookup_icon ("game-symbolic", null, (int) size, (int) size, get_direction (), 0);
	}

	private void get_cover (DrawingContext context, out Gdk.Paintable cover) {
		var cover_size = int.min (context.width, context.height);

		if (cover_size != last_cover_size || scale_factor != last_scale_factor) {
			cover_paintable = null;
			update_style_classes ();
			try_load_cover = true;
		}

		if (!try_load_cover) {
			cover = cover_paintable;
			return;
		}

		var loader = Application.get_default ().get_cover_loader ();

		last_cover_size = cover_size;
		last_scale_factor = scale_factor;

		try_load_cover = false;
		loader.fetch_cover (game, scale_factor, cover_size, (scale_factor, cover_size, cover_paintable) => {
			if (scale_factor != last_scale_factor || cover_size != last_cover_size) {
				this.cover_paintable = null;

				try_load_cover = true;
			}
			else if (cover_paintable != null)
				this.cover_paintable = cover_paintable;

			update_style_classes ();

			queue_draw ();
		});

		cover = cover_paintable;
	}

	private void draw_paintable (DrawingContext context, Gdk.Paintable paintable) {
		context.snapshot.save ();

		float w = paintable.get_intrinsic_width ();
		float h = paintable.get_intrinsic_height ();

		context.snapshot.scale (1.0f / scale_factor, 1.0f / scale_factor);
		context.snapshot.translate ({
			(context.width * scale_factor - w) / 2.0f,
			(context.height * scale_factor - h) / 2.0f,
		});

		paintable.snapshot (context.snapshot, w, h);
		context.snapshot.restore ();
	}
}
