// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/collection-thumbnail.ui")]
private class Highscore.CollectionThumbnail : Adw.Bin {
	// This should match the number of grid children in the template
	const uint N_ROWS = 2;
	const uint N_COLUMNS = 2;

	[GtkChild]
	private unowned Gtk.Stack thumbnail_stack;
	[GtkChild]
	private unowned Adw.Bin bin_1;
	[GtkChild]
	private unowned Adw.Bin bin_2;
	[GtkChild]
	private unowned Adw.Bin bin_3;
	[GtkChild]
	private unowned Adw.Bin bin_4;
	[GtkChild]
	private unowned Gtk.Image new_collection_image;

	private ulong games_changed_id = 0;

	private Collection _collection;
	public Collection collection {
		get { return _collection; }
		set {
			if (games_changed_id > 0)
				collection.disconnect (games_changed_id);

			_collection = value;

			if (collection.get_collection_type () == CollectionType.PLACEHOLDER) {
				thumbnail_stack.visible_child = new_collection_image;
				return;
			}

			games_changed_id = collection.games_changed.connect (on_games_changed);
			on_games_changed ();
		}
	}

	private void on_games_changed () {
		var max_subcovers = N_ROWS * N_COLUMNS;

		Adw.Bin[] children = { bin_1, bin_2, bin_3, bin_4 };

		var pos = 0;
		var game_model = collection.get_game_model ();
		var n_games = game_model.get_n_items ();
		foreach (var bin in children) {
			if (pos < n_games) {
				var game = game_model.get_item (pos) as Game;
				bin.child = get_game_thumbnail (game);
				pos++;

				continue;
			}

			if (pos < max_subcovers) {
				bin.child = null;
				pos++;
			}
		}
	}

	private GameThumbnail? get_game_thumbnail (Game game) {
		var game_thumbnail = new GameThumbnail ();
		game_thumbnail.game = game;
		game.replaced.connect ((new_game) => {
			game_thumbnail.game = new_game;
		});
		game_thumbnail.visible = true;

		return game_thumbnail;
	}
}
