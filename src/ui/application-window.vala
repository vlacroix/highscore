// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/application-window.ui")]
private class Highscore.ApplicationWindow : Adw.ApplicationWindow {
	private const uint WINDOW_SIZE_UPDATE_DELAY_MILLISECONDS = 500;

	[GtkChild]
	private unowned Gtk.Stack stack;
	[GtkChild]
	private unowned CollectionView collection_view;
	[GtkChild]
	private unowned DisplayView display_view;

	private UiView _current_view;
	public UiView current_view {
		get { return _current_view; }
		set {
			if (value == current_view)
				return;

			if (current_view != null)
				current_view.is_view_active = false;

			_current_view = value;

			stack.visible_child = current_view;

			if (current_view != null)
				current_view.is_view_active = true;

			var app = application as Application;
			assert (app != null);

			app.set_pause_loading (current_view != collection_view);
		}
	}

	private Settings settings;

	private long window_size_update_timeout;

	private uint inhibit_cookie;
	private Gtk.ApplicationInhibitFlags inhibit_flags;

	public GameModel game_model { get; construct; }
	public CollectionModel collection_model { get; construct; }

	private bool confirm_exit;

	public ApplicationWindow (Application application, GameModel game_model, CollectionModel collection_model) {
		Object (application: application, game_model: game_model, collection_model: collection_model);

		current_view = collection_view;
	}

	construct {
		settings = new Settings ("org.gnome.World.Highscore");

		collection_view.game_model = game_model;
		collection_view.collection_model = collection_model;

		int width, height;
		settings.get ("window-size", "(ii)", out width, out height);
		default_width = width;
		default_height = height;

		if (settings.get_boolean ("window-maximized"))
			maximize ();

		window_size_update_timeout = -1;
		inhibit_cookie = 0;
		inhibit_flags = 0;

		if (Config.PROFILE == "Devel")
			add_css_class ("devel");

		init_help_overlay ();
	}

	private void init_help_overlay () {
		var builder = new Gtk.Builder.from_resource ("/org/gnome/World/Highscore/ui/help-overlay.ui");
		var shortcuts_window = builder.get_object ("help_overlay") as Gtk.ShortcutsWindow;
		var shortcut = builder.get_object ("general_shortcut_alt_left") as Gtk.ShortcutsShortcut;

		shortcuts_window.direction_changed.connect (() => {
			shortcut.accelerator = get_alt_left_right ();
		});
		shortcut.accelerator = get_alt_left_right ();

		set_help_overlay (shortcuts_window);
	}

	private string get_alt_left_right () {
		return get_direction () == Gtk.TextDirection.LTR ? "<alt>Left" : "<alt>Right";
	}

	public void run_search (string query) {
		if (current_view != collection_view)
			return;

		collection_view.run_search (query);
	}

	public void show_error (string error_message) {
		collection_view.show_error (error_message);
	}

	public async void run_game (Game game) {
		if (current_view != collection_view)
			return;

		current_view = display_view;
		yield display_view.run_game (game);

		inhibit (Gtk.ApplicationInhibitFlags.IDLE | Gtk.ApplicationInhibitFlags.LOGOUT);
	}

	public async bool quit_game () {
		// If the window have been deleted/hidden we probably don't want to
		// prompt the user.
		if (!visible)
			return true;

		return yield display_view.quit_game ();
	}

	protected override bool close_request () {
		if (confirm_exit)
			return Gdk.EVENT_PROPAGATE;

		quit_game.begin ((obj, res) => {
			if (!quit_game.end (res))
				return;

			confirm_exit = true;

			close ();
		});

		return Gdk.EVENT_STOP;
	}

	public bool gamepad_button_press_event (Manette.Event event) {
		return current_view.gamepad_button_press_event (event);
	}

	public bool gamepad_button_release_event (Manette.Event event) {
		if (current_view == collection_view)
			return collection_view.gamepad_button_release_event (event);

		return false;
	}

	public bool gamepad_absolute_axis_event (Manette.Event event) {
		if (current_view == collection_view)
			return collection_view.gamepad_absolute_axis_event (event);

		return false;
	}

	[GtkCallback]
	private void on_game_activated (Game game) {
		run_game.begin (game);
	}

	[GtkCallback]
	private void on_display_back () {
		quit_game.begin ((obj, res) => {
			if (quit_game.end (res))
				current_view = collection_view;

			uninhibit (Gtk.ApplicationInhibitFlags.IDLE | Gtk.ApplicationInhibitFlags.LOGOUT);
		});
	}

	[GtkCallback]
	private void on_active_changed () {
		var playing = (current_view == display_view);

		if (playing)
			display_view.update_pause ();

		if (is_active && playing)
			inhibit (Gtk.ApplicationInhibitFlags.IDLE);

		if (!is_active)
			uninhibit (Gtk.ApplicationInhibitFlags.IDLE);
	}

	[GtkCallback]
	private void window_state_changed () {
		settings.set_boolean ("window-maximized", maximized);

		if (window_size_update_timeout == -1 && !maximized)
			window_size_update_timeout = Timeout.add (WINDOW_SIZE_UPDATE_DELAY_MILLISECONDS, store_window_size);
	}

	private bool store_window_size () {
		settings.set ("window-size", "(ii)", default_width, default_height);

		Source.remove ((uint) window_size_update_timeout);
		window_size_update_timeout = -1;

		return false;
	}

	private void inhibit (Gtk.ApplicationInhibitFlags flags) {
		if ((inhibit_flags & flags) == flags)
			return;

		Gtk.ApplicationInhibitFlags new_flags = (inhibit_flags | flags);
		/* Translators: This is displayed if the user tries to log out of his
		 * GNOME session, shutdown, or reboot while Highscore is running */
		uint new_cookie = application.inhibit (this, new_flags, _("Playing a game"));

		if (inhibit_cookie != 0)
			application.uninhibit (inhibit_cookie);

		inhibit_cookie = new_cookie;
		inhibit_flags = new_flags;
	}

	private void uninhibit (Gtk.ApplicationInhibitFlags flags) {
		if ((inhibit_flags & flags) == 0)
			return;

		Gtk.ApplicationInhibitFlags new_flags = (inhibit_flags & ~flags);
		uint new_cookie = 0;

		if ((bool) new_flags)
			/* Translators: This is displayed if the user tries to log out of his
		 	 * GNOME session, shutdown, or reboot while Highscore is running */
			new_cookie = application.inhibit (this, new_flags, _("Playing a game"));

		if (inhibit_cookie != 0)
			application.uninhibit (inhibit_cookie);

		inhibit_cookie = new_cookie;
		inhibit_flags = new_flags;
	}
}
