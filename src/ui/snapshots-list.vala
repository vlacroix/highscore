// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/snapshots-list.ui")]
private class Highscore.SnapshotsList : Gtk.Box {
	[GtkChild]
	private unowned Gtk.ListBox list_box;
	[GtkChild]
	private unowned Gtk.ListBoxRow new_snapshot_row;
	[GtkChild]
	private unowned Gtk.ScrolledWindow scrolled_window;

	private Snapshot selected_snapshot;

	public Runner runner { get; set; }
	public bool has_snapshot_selected { get; set; default = false; }

	static construct {
		install_action ("snapshots.delete", null, widget => {
			var self = widget as SnapshotsList;

			self.do_delete ();
		});

		install_action ("snapshots.rename", null, widget => {
			var self = widget as SnapshotsList;

			self.do_rename ();
		});

		add_binding_action (Gdk.Key.Delete, 0, "snapshots.delete", null);
	}

	[GtkCallback]
	private void on_move_cursor () {
		var row = list_box.get_selected_row ();

		if (row != null && row is SnapshotRow) {
			var snapshot_row = row as SnapshotRow;
			var snapshot = snapshot_row.game_snapshot;

			if (snapshot != selected_snapshot)
				select_snapshot_row (row);
		}
	}

	[GtkCallback]
	private void on_row_activated (Gtk.ListBoxRow activated_row) {
		if (activated_row == new_snapshot_row) {
			var snapshot = runner.try_create_snapshot (false);

			if (snapshot != null) {
				var snapshot_row = new SnapshotRow (runner, snapshot);

				list_box.insert (snapshot_row, 1);
				select_snapshot_row (snapshot_row);
				snapshot_row.reveal ();
			}
			else {
				select_snapshot_row (list_box.get_row_at_index (1));

				// TODO: Add a warning
			}
		} else
			select_snapshot_row (activated_row);
	}

	private void populate_list_box () {
		while (true) {
			var row = list_box.get_row_at_index (1);

			if (row == null)
				break;

			list_box.remove (row);
		}

		if (runner == null)
			return;

		var snapshots = _runner.get_snapshots ();
		foreach (var snapshot in snapshots) {
			var list_row = new SnapshotRow (runner, snapshot);

			// Reveal it early so that it doesn't animate
			list_row.reveal ();
			list_box.append (list_row);
		}
	}

	public void reveal () {
		runner.pause ();
		populate_list_box ();
		select_snapshot_row (null);
	}

	private void do_delete () {
		var selected_row = list_box.get_selected_row ();
		var selected_row_index = selected_row.get_index ();
		var snapshot_row = selected_row as SnapshotRow;
		var snapshot = snapshot_row.game_snapshot;

		ensure_row_is_visible (selected_row);
		runner.delete_snapshot (snapshot);

		// Select and preview a new row
		var next_row_index = selected_row_index + 1;
		var new_selected_row = list_box.get_row_at_index (next_row_index);
		while (new_selected_row != null && !new_selected_row.selectable) {
			next_row_index++;
			new_selected_row = list_box.get_row_at_index (next_row_index);
		}

		if (new_selected_row == null) {
			// There are no more selectable rows after the selected row
			// Check if there are any selectable rows before the selected row

			var prev_row_index = selected_row_index - 1;
			new_selected_row = list_box.get_row_at_index (prev_row_index);
			while (prev_row_index > 1 && !new_selected_row.selectable) {
				prev_row_index--;
				new_selected_row = list_box.get_row_at_index (prev_row_index);
			}
		}

		if (new_selected_row != null && new_selected_row.selectable)
			select_snapshot_row (new_selected_row);
		else
			select_snapshot_row (null);

		snapshot_row.remove_animated ();
	}

	private void do_rename () {
		var selected_row = list_box.get_selected_row () as SnapshotRow;

		ensure_row_is_visible (selected_row);

		selected_row.start_rename ();
	}

	// Adapted from gtklistbox.c, ensure_row_visible()
	private void ensure_row_is_visible (Gtk.ListBoxRow row) {
		Gtk.Allocation allocation;

		row.get_allocation (out allocation);
		var y = allocation.y;
		var height = allocation.height;

		scrolled_window.kinetic_scrolling = false;
		scrolled_window.vadjustment.clamp_page (y, y + height);
		scrolled_window.kinetic_scrolling = true;
	}

	private void select_snapshot_row (Gtk.ListBoxRow? row) {
		list_box.select_row (row);

		if (row == null) {
			runner.preview_current_state ();
			selected_snapshot = null;
			has_snapshot_selected = false;
		}
		else {
			row.grab_focus ();

			if (!(row is SnapshotRow))
				return;

			var snapshot_row = row as SnapshotRow;
			var snapshot = snapshot_row.game_snapshot;

			if (snapshot == selected_snapshot) {
				activate_action ("display.load-snapshot", "()");

				return;
			}

			runner.preview_snapshot (snapshot);
			selected_snapshot = snapshot;
			has_snapshot_selected = true;
		}

		action_set_enabled ("snapshots.delete", selected_snapshot != null);
		action_set_enabled ("snapshots.rename", selected_snapshot != null &&
		                                        !selected_snapshot.is_automatic);
	}
}
