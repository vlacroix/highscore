// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/checkmark-item.ui")]
private class Highscore.CheckmarkItem : Gtk.ListBoxRow {
	public bool checkmark_visible { get; set; default = false; }
	public string label { get; construct; }

	public CheckmarkItem (string label) {
		Object (label: label);
	}
}
