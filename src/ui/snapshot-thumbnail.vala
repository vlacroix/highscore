// This file is part of Highscore. License: GPL-3.0+.

private class Highscore.SnapshotThumbnail : Gtk.Widget {
	public const int THUMBNAIL_SIZE = 64;

	private Snapshot _game_snapshot;
	public Snapshot game_snapshot {
		get { return _game_snapshot; }
		set {
			_game_snapshot = value;

			load_thumbnail ();
		}
	}

	private Gdk.Texture texture;

	construct {
		width_request = THUMBNAIL_SIZE;
		height_request = THUMBNAIL_SIZE;

		add_css_class ("snapshot-thumbnail");

		notify["scale-factor"].connect (load_thumbnail);
	}

	private void load_thumbnail () {
		if (game_snapshot == null)
			return;

		var screenshot_path = game_snapshot.get_screenshot_path ();

		try {
			texture = Gdk.Texture.from_file (File.new_for_path (screenshot_path));
		}
		catch (Error e) {
			warning ("Failed to load snapshot thumbnail: %s", e.message);
		}
	}

	public override void snapshot (Gtk.Snapshot snapshot) {
		var width = get_width ();
		var height = get_height ();

		if (texture == null)
			return;

		var aspect_ratio = game_snapshot.screenshot_aspect_ratio;

		// A fallback for migrated snapshots
		if (aspect_ratio == 0)
			aspect_ratio = texture.get_intrinsic_aspect_ratio ();

		var thumbnail_width = texture.get_intrinsic_width ();
		var thumbnail_height = (int) (thumbnail_width / aspect_ratio);

		if (thumbnail_width > thumbnail_height) {
			thumbnail_width = THUMBNAIL_SIZE;
			thumbnail_height = (int) (THUMBNAIL_SIZE / aspect_ratio);
		}
		else {
			thumbnail_height = THUMBNAIL_SIZE;
			thumbnail_width = (int) (THUMBNAIL_SIZE * aspect_ratio);
		}

		thumbnail_width *= scale_factor;
		thumbnail_height *= scale_factor;

		snapshot.scale (1.0f / scale_factor, 1.0f / scale_factor);

		var x_offset = (width * scale_factor - thumbnail_width) / 2;
		var y_offset = (height * scale_factor - thumbnail_height) / 2;

		snapshot.append_texture (texture, {
			{ x_offset, y_offset },
			{ thumbnail_width, thumbnail_height }
		});
	}
}
