// This file is part of Highscore. License: GPL-3.0+.

private class Highscore.PopoverBin : Gtk.Widget, Gtk.Buildable {
	private Gtk.Widget? _child;
	public Gtk.Widget? child {
		get { return _child; }
		set {
			if (child == value)
				return;

			if (child != null)
				child.unparent ();

			_child = value;

			if (child != null)
				child.set_parent (this);
		}
	}

	private Gtk.Popover? _popover;
	public Gtk.Popover? popover {
		get { return _popover; }
		set {
			if (popover == value)
				return;

			if (popover != null)
				popover.unparent ();

			_popover = value;

			if (popover != null)
				popover.set_parent (this);
		}
	}

	public void add_child (Gtk.Builder builder, Object child, string? type) {
		if (child is Gtk.Popover && type == "popover") {
			popover = child as Gtk.Popover;
			return;
		}

		if (child is Gtk.Widget) {
			this.child = child as Gtk.Widget;
			return;
		}

		base.add_child (builder, child, type);
	}

	protected override void measure (Gtk.Orientation orientation, int for_size, out int min, out int nat, out int min_baseline, out int nat_baseline) {
		if (child == null) {
			min = 0;
			nat = 0;
			min_baseline = -1;
			nat_baseline = -1;

			return;
		}

		child.measure (orientation, for_size, out min, out nat, out min_baseline, out nat_baseline);
	}

	protected override void size_allocate (int width, int height, int baseline) {
		if (child != null)
			child.allocate (width, height, baseline, null);

		if (popover != null)
			popover.present ();
	}

	protected override void dispose () {
		child = null;
		popover = null;

		base.dispose ();
	}
}
