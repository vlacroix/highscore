// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/snapshot-row.ui")]
private class Highscore.SnapshotRow : Gtk.ListBoxRow {
	[GtkChild]
	private unowned SnapshotThumbnail thumbnail;
	[GtkChild]
	private unowned Gtk.Label name_label;
	[GtkChild]
	private unowned Gtk.Label date_label;
	[GtkChild]
	private unowned Gtk.Revealer revealer;
	[GtkChild]
	private unowned Gtk.Popover rename_popover;
	[GtkChild]
	private unowned Gtk.Entry rename_entry;
	[GtkChild]
	private unowned Gtk.Button rename_popover_btn;
	[GtkChild]
	private unowned Gtk.Label rename_error_label;

	public Runner runner { get; set; }

	private Snapshot _game_snapshot;
	public Snapshot game_snapshot {
		get { return _game_snapshot; }
		set {
			_game_snapshot = value;

			if (game_snapshot.is_automatic)
				name_label.label = _("Autosave");
			else
				name_label.label = game_snapshot.name;

			var creation_date = game_snapshot.creation_date;
			var date_format = get_date_format (creation_date);
			date_label.label = creation_date.format (date_format);

			thumbnail.game_snapshot = game_snapshot;
		}
	}

	public SnapshotRow (Runner runner, Snapshot game_snapshot) {
		Object (runner: runner, game_snapshot: game_snapshot);
	}

	static construct {
		typeof (PopoverBin).ensure ();
	}

	public void reveal () {
		revealer.reveal_child = true;
	}

	public void remove_animated () {
		selectable = false;
		revealer.notify["child-revealed"].connect (() => {
			var listbox = get_parent () as Gtk.ListBox;
			listbox.remove (this);
		});
		revealer.reveal_child = false;
	}

	// Adapted from nautilus-file.c, nautilus_file_get_date_as_string()
	private string get_date_format (DateTime date) {
		var date_midnight = new DateTime.local (date.get_year (),
		                                        date.get_month (),
		                                        date.get_day_of_month (),
		                                        0, 0, 0);
		var now = new DateTime.now ();
		var today_midnight = new DateTime.local (now.get_year (), now.get_month (), now.get_day_of_month (), 0, 0, 0);
		var days_ago = (today_midnight.difference (date_midnight)) / GLib.TimeSpan.DAY;

		if (days_ago == 0) {
			/* Translators: Time in locale format */
			/* xgettext:no-c-format */
			return _("%X");
		}
		else if (days_ago == 1) {
			/* Translators: this is the word Yesterday followed by
			 * a time in locale format. i.e. "Yesterday 23:04:35" */
			/* xgettext:no-c-format */
			return _("Yesterday %X");
		}
		else if (days_ago > 1 && days_ago < 7) {
			/* Translators: this is the abbreviated name of the week day followed by
			 * a time in locale format. i.e. "Monday 23:04:35" */
			/* xgettext:no-c-format */
			return _("%a %X");
		}
		else if (date.get_year () == now.get_year ()) {
			/* Translators: this is the day of the month followed
			 * by the abbreviated month name followed by a time in
			 * locale format i.e. "3 Feb 23:04:35" */
			/* xgettext:no-c-format */
			return _("%-e %b %X");
		}
		else {
			/* Translators: this is the day number followed
			 * by the abbreviated month name followed by the year followed
			 * by a time in locale format i.e. "3 Feb 2015 23:04:00" */
			/* xgettext:no-c-format */
			return _("%-e %b %Y %X");
		}
	}

	[GtkCallback]
	private void on_rename_entry_activated () {
		if (check_rename_is_valid ())
			apply_rename ();
	}

	[GtkCallback]
	private void on_rename_entry_text_changed () {
		check_rename_is_valid ();
	}

	private bool check_rename_is_valid () {
		var entry_text = rename_entry.text.strip ();

		if (entry_text == _("Autosave") || entry_text == "") {
			rename_entry.add_css_class ("error");
			rename_popover_btn.sensitive = false;

			/* Translators: This message is shown to the user if he tried to rename
			 * his snapshot either with an empty string, or with the name of the
			 * autosave */
			rename_error_label.label = _("Invalid name");

			return false;
		}

		var snapshots = runner.get_snapshots ();
		foreach (var snapshot in snapshots) {
			if (snapshot.is_automatic)
				continue;

			if (snapshot.name == entry_text) {
				rename_entry.add_css_class ("error");
				rename_popover_btn.sensitive = false;
				rename_error_label.label =_("A snapshot with this name already exists");

				return false;
			}
		}

		// All checks passed, rename operation is valid
		rename_entry.remove_css_class ("error");
		rename_popover_btn.sensitive = true;
		rename_error_label.label = "";

		return true;
	}

	[GtkCallback]
	private void apply_rename () {
		game_snapshot.name = rename_entry.text.strip ();
		name_label.label = game_snapshot.name;

		try {
			game_snapshot.write_metadata ();
		}
		catch (Error e) {
			critical ("Couldn't update snapshot name: %s", e.message);
		}

		rename_popover.popdown ();
	}

	public void start_rename () {
		rename_entry.text = game_snapshot.name;
		rename_popover.popup ();
	}
}
