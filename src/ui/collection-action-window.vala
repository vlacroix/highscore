// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/collection-action-window.ui")]
private class Highscore.CollectionActionWindow : Adw.Window {
	public signal void confirmed (Collection[] collections);

	[GtkChild]
	private unowned Adw.Leaflet leaflet;
	[GtkChild]
	private unowned Gtk.Box add_to_collection_page;
	[GtkChild]
	private unowned Gtk.Box create_collection_page;
	[GtkChild]
	private unowned Gtk.Stack user_collections_page_stack;
	[GtkChild]
	private unowned Gtk.ScrolledWindow list_page;
	[GtkChild]
	private unowned Adw.StatusPage empty_page;
	[GtkChild]
	private unowned Gtk.Entry name_entry;
	[GtkChild]
	private unowned Gtk.Label error_label;
	[GtkChild]
	private unowned Gtk.ListBox list_box;
	[GtkChild]
	private unowned Gtk.SearchBar search_bar;
	[GtkChild]
	private unowned Gtk.SearchEntry search_entry;
	[GtkChild]
	private unowned Gtk.ListBoxRow add_row;

	private CollectionManager collection_manager;

	public string[] filtering_terms;
	private string filtering_text {
		set {
			if (value == null)
				filtering_terms = null;
			else
				filtering_terms = value.split (" ");

			list_box.invalidate_filter ();
		}
	}

	private CollectionModel _collection_model;
	public CollectionModel collection_model {
		get { return _collection_model; }
		set {
			_collection_model = value;

			list_box.bind_model (collection_model, add_collection_row);
			list_box.set_filter_func (list_box_filter);
			list_box.invalidate_filter ();
		}
	}

	private bool _is_user_collections_empty;
	public bool is_user_collections_empty {
		get { return _is_user_collections_empty; }
		set {
			_is_user_collections_empty = value;

			if (is_user_collections_empty)
				user_collections_page_stack.visible_child = empty_page;
			else
				user_collections_page_stack.visible_child = list_page;

			update_actions ();
		}
	}

	public bool is_search_mode { get; set; }
	public bool is_collection_name_valid { get; set; }
	public bool create_collection_page_only { get; construct; }
	public Collection insensitive_collection { get; construct; }

	construct {
		if (create_collection_page_only)
			leaflet.visible_child = create_collection_page;
		else
			leaflet.visible_child = add_to_collection_page;

		collection_manager = Application.get_default ().get_collection_manager ();

		is_user_collections_empty = collection_manager.n_user_collections == 0;
		collection_manager.collection_added.connect ((collection) => {
			is_user_collections_empty = collection_manager.n_user_collections == 0;
		});

		search_bar.connect_entry (search_entry);

		update_actions ();
	}

	static construct {
		install_action ("window.back", null, widget => {
			var self = widget as CollectionActionWindow;

			self.leaflet.navigate (Adw.NavigationDirection.BACK);
		});

		install_action ("window.new-collection", null, widget => {
			var self = widget as CollectionActionWindow;

			self.leaflet.navigate (Adw.NavigationDirection.FORWARD);
		});

		install_action ("window.create-collection", null, widget => {
			var self = widget as CollectionActionWindow;

			self.create_collection ();
		});

		install_action ("window.add-to-collection", null, widget => {
			var self = widget as CollectionActionWindow;

			self.add_to_collection ();
		});

		install_property_action ("window.search", "is-search-mode");

		add_binding_action (Gdk.Key.f, Gdk.ModifierType.CONTROL_MASK,
		                    "window.search", null);

		add_binding (Gdk.Key.Left, Gdk.ModifierType.ALT_MASK, widget => {
			var self = widget as CollectionActionWindow;

			if (self.get_direction () == Gtk.TextDirection.RTL)
				return Gdk.EVENT_PROPAGATE;

			return self.activate_action ("window.back", "()");
		}, null);

		add_binding (Gdk.Key.Right, Gdk.ModifierType.ALT_MASK, widget => {
			var self = widget as CollectionActionWindow;

			if (self.get_direction () == Gtk.TextDirection.LTR)
				return Gdk.EVENT_PROPAGATE;

			return self.activate_action ("window.back", "()");
		}, null);

		add_binding (Gdk.Key.Escape, 0, widget => {
			var self = widget as CollectionActionWindow;

			if (self.create_collection_page_only) {
				self.close ();
				return Gdk.EVENT_STOP;
			}

			if (self.leaflet.navigate (Adw.NavigationDirection.BACK))
				return Gdk.EVENT_STOP;

			if (self.is_search_mode) {
				self.is_search_mode = false;
				return Gdk.EVENT_STOP;
			}

			self.close ();

			return Gdk.EVENT_STOP;
		}, null);
	}

	public CollectionActionWindow (bool create_collection_page_only = true, Collection? collection = null) {
		Object (create_collection_page_only : create_collection_page_only, insensitive_collection : collection);
	}

	private Gtk.Widget add_collection_row (Object object) {
		var collection = object as Collection;
		if (collection.get_collection_type () == CollectionType.PLACEHOLDER)
			return add_row;

		var row = new CollectionListItem (collection);
		row.sensitive = collection != insensitive_collection;
		row.show ();

		return row;
	}

	private bool list_box_filter (Gtk.ListBoxRow row) {
		bool show_row;

		if (row is CollectionListItem) {
			var list_item = row as CollectionListItem;
			var collection = list_item.collection;
			var type = collection.get_collection_type ();

			show_row = (type == CollectionType.USER) &&
				       ((is_search_mode && collection.matches_search_terms (filtering_terms)) ||
				        (!is_search_mode));
		}
		else
			show_row = !is_search_mode || filtering_terms.length == 0;

		row.visible = show_row;
		return show_row;
	}

	private void add_to_collection () {
		Collection[] collections = {};

		int i = 0;
		while (true) {
			var row = list_box.get_row_at_index (i);

			if (row == null || !(row is CollectionListItem))
				break;

			i++;

			var collection_row = row as CollectionListItem;

			if (collection_row.collection.get_collection_type () != CollectionType.USER)
				continue;

			var check_button = collection_row.activatable_widget as Gtk.CheckButton;
			if (check_button.active)
				collections += collection_row.collection;
		}

		confirmed (collections);
		close ();
	}

	[GtkCallback]
	private void create_collection () {
		if (!is_collection_name_valid)
			return;

		collection_manager.create_user_collection (name_entry.text.strip ());

		if (create_collection_page_only)
			close ();
		else
			leaflet.navigate (Adw.NavigationDirection.BACK);
	}

	[GtkCallback]
	private bool key_pressed_cb (uint keyval, uint keycode, Gdk.ModifierType state) {
		return Gdk.EVENT_PROPAGATE;
	}

	[GtkCallback]
	private void on_collection_name_entry_changed () {
		var name = name_entry.text.strip ();
		if (name == "") {
			error_label.label = _("Collection name cannot be empty");
			name_entry.add_css_class ("error");
			is_collection_name_valid = false;
			return;
		}

		if (collection_manager.does_collection_title_exist (name)) {
			error_label.label = _("A collection with this name already exists");
			name_entry.add_css_class ("error");
			is_collection_name_valid = false;
			return;
		}

		name_entry.remove_css_class ("error");
		error_label.label = null;
		is_collection_name_valid = true;
	}

	[GtkCallback]
	private void on_visible_child_changed () {
		if (leaflet.visible_child == create_collection_page) {
			name_entry.text = "";
			error_label.label = "";
			name_entry.remove_css_class ("error");
			name_entry.grab_focus ();
		}

		update_actions ();
	}

	[GtkCallback]
	private void on_search_text_notify () {
		filtering_text = search_entry.text;
		search_entry.grab_focus ();
	}

	private void update_actions () {
		action_set_enabled ("window.back", !create_collection_page_only);
		action_set_enabled ("window.search",
		                    !is_user_collections_empty &&
		                    leaflet.visible_child == add_to_collection_page);
	}
}
