// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/collection-icon-view.ui")]
private class Highscore.CollectionIconView : Gtk.FlowBoxChild {
	public signal void secondary_click ();

	[GtkChild]
	private unowned Gtk.Label title;
	[GtkChild]
	private unowned CollectionThumbnail thumbnail;

	public bool checked { get; set; }
	public bool is_selection_mode { get; set; }

	private Collection _collection;
	public Collection collection {
		get { return _collection; }
		construct set {
			_collection = value;

			title.label = collection.title;
			thumbnail.collection = collection;
		}
	}

	construct {
		add_css_class ("collection-icon-view");

		var click_gesture = new Gtk.GestureClick ();
		click_gesture.button = 3;
		click_gesture.pressed.connect (() => {
			var event = click_gesture.get_current_event ();

			if (event.triggers_context_menu ()) {
				secondary_click ();
				click_gesture.set_state (Gtk.EventSequenceState.CLAIMED);
			} else {
				click_gesture.set_state (Gtk.EventSequenceState.DENIED);
			}
		});
		add_controller (click_gesture);

		var long_press_gesture = new Gtk.GestureLongPress ();
		long_press_gesture.pressed.connect (() => {
			secondary_click ();
			long_press_gesture.set_state (Gtk.EventSequenceState.CLAIMED);
		});
		add_controller (long_press_gesture);
	}

	public CollectionIconView (Collection collection) {
		Object (collection: collection);
	}

	public static uint hash (CollectionIconView key) {
		return Collection.hash (key.collection);
	}

	public static bool equal (CollectionIconView a, CollectionIconView b) {
		return Collection.equal (a.collection, b.collection);
	}
}
