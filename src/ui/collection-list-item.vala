// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/collection-list-item.ui")]
private class Highscore.CollectionListItem : Adw.ActionRow {
	public Collection collection { get; construct; }

	construct {
		title = collection.title;
	}

	public CollectionListItem (Collection collection) {
		Object (collection: collection);
	}
}
