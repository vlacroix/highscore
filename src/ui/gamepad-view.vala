// This file is part of Highscore. License: GPL-3.0+.

private class Highscore.GamepadView : Gtk.DrawingArea {
	private struct InputState {
		bool highlight;
		double offset_x;
		double offset_y;
	}

	private Rsvg.Handle handle;
	private new HashTable<string, InputState?> input_state;

	private GamepadViewConfiguration _configuration;
	public GamepadViewConfiguration configuration {
		get { return _configuration; }
		set {
			if (value == configuration)
				return;

			_configuration = value;

			if (value.svg_path == "")
				return;

			try {
				var bytes = resources_lookup_data (value.svg_path, ResourceLookupFlags.NONE);
				var data = bytes.get_data ();

				handle = new Rsvg.Handle.from_data (data);
			}
			catch (Error e) {
				critical ("Could not set up gamepad view: %s", e.message);
			}

			double width, height;
			get_dimensions (out width, out height);

			set_size_request ((int) width, (int) height);

			input_state.foreach_remove (() => true);

			foreach (var path in configuration.button_paths) {
				if (path.path in input_state)
					continue;

				input_state[path.path] = {};
			}

			foreach (var path in configuration.analog_paths) {
				if (path.path in input_state)
					continue;

				input_state[path.path] = {};
			}

			reset ();
		}
	}

	construct {
		handle = new Rsvg.Handle ();
		configuration = { "", new GamepadButtonPath[0] };
		input_state = new HashTable<string, InputState?> (str_hash, str_equal);

		set_draw_func (draw_cb);
	}

	private void get_dimensions (out double width, out double height) {
		bool has_width, has_height, has_viewbox;
		Rsvg.Length handle_width, handle_height;
		Rsvg.Rectangle viewbox;

		handle.get_intrinsic_dimensions (out has_width, out handle_width,
		                                 out has_height, out handle_height,
		                                 out has_viewbox, out viewbox);

		assert (has_width && has_height);

		width = handle_width.length;
		height = handle_height.length;
	}

	public void reset () {
		input_state.foreach ((path, state) => {
			state.highlight = false;
			state.offset_x = 0;
			state.offset_y = 0;
		});

		queue_draw ();
	}

	public bool highlight (GamepadInput input, bool highlight) {
		foreach (var path in configuration.button_paths) {
			if (input != path.input)
				continue;

			input_state[path.path].highlight = highlight;

			queue_draw ();

			return true;
		}

		return false;
	}

	public bool set_analog (GamepadInput input, double value) {
		foreach (var path in configuration.analog_paths) {
			if (input != path.input_x && input != path.input_y)
				continue;

			if (input == path.input_x)
				input_state[path.path].offset_x = value * path.offset_radius;
			else
				input_state[path.path].offset_y = value * path.offset_radius;

			queue_draw ();

			return true;
		}

		return false;
	}

	int ii;

	private void draw_cb (Gtk.DrawingArea area, Cairo.Context cr, int width, int height) {
		double x, y, scale;
		calculate_image_dimensions (width, height, out x, out y, out scale);

		cr.translate (x, y);
		cr.scale (scale, scale);

		var context = get_style_context ();
		ii = 0;

		foreach (var path in configuration.background_paths) {
			var color = context.get_color ();

			draw_path (cr, path, color, 0, 0);
			ii++;
		}

		input_state.for_each ((path, state) => {
			Gdk.RGBA color;

			if (state.highlight)
				context.lookup_color ("accent_color", out color);
			else
				color = context.get_color ();

			draw_path (cr, path, color, state.offset_x, state.offset_y);
			ii++;
		});
	}

	private void draw_path (Cairo.Context cr, string path, Gdk.RGBA color, double offset_x, double offset_y) {
		cr.push_group ();

		cr.translate (offset_x, offset_y);
		handle.render_cairo_sub (cr, @"#$path");

		var group = cr.pop_group ();

		Gdk.cairo_set_source_rgba (cr, color);
		cr.mask (group);
	}

	private void calculate_image_dimensions (int width, int height, out double x, out double y, out double scale) {
		double image_width, image_height;
		get_dimensions (out image_width, out image_height);

		scale = double.min ((double) height / image_height, (double) width / image_width);

		x = (width - image_width * scale) / 2;
		y = (height - image_height * scale) / 2;
	}
}
