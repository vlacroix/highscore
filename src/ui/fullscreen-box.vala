// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore/ui/fullscreen-box.ui")]
private class Highscore.FullscreenBox : Adw.Bin, Gtk.Buildable {
	private const uint INACTIVITY_TIME_MILLISECONDS = 3000;
	private const int SHOW_HEADERBAR_DISTANCE = 5;

	[GtkChild]
	private unowned Adw.Flap flap;

	private bool _is_fullscreen;
	public bool is_fullscreen {
		get { return _is_fullscreen; }
		set {
			_is_fullscreen = value;

			if (!autohide)
				return;

			if (is_fullscreen) {
				show_ui ();
				on_cursor_moved ();
			}
			else
				on_restore ();
		}
	}

	private bool _overlay;
	public bool overlay {
		get { return _overlay; }
		set {
			if (value == overlay)
				return;

			_overlay = value;

			if (flap == null)
				return;

			flap.fold_policy = overlay ? Adw.FlapFoldPolicy.ALWAYS : Adw.FlapFoldPolicy.NEVER;
		}
	}

	private bool _autohide = true;
	public bool autohide {
		get { return _autohide; }
		set {
			if (autohide == value)
				return;

			_autohide = value;

			if (value) {
				show_ui ();
				on_cursor_moved ();
			}
			else {
				// Disable timers
				if (ui_timeout_id != -1) {
					Source.remove (ui_timeout_id);
					ui_timeout_id = -1;
				}

				if (cursor_timeout_id != -1) {
					Source.remove (cursor_timeout_id);
					cursor_timeout_id = -1;
				}

				flap.reveal_flap = true;
				show_cursor (true);
			}
		}
	}

	private uint ui_timeout_id;
	private uint cursor_timeout_id;

	construct {
		ui_timeout_id = -1;
		cursor_timeout_id = -1;
	}

	public void add_child (Gtk.Builder builder, Object child, string? type) {
		if (!(child is Gtk.Widget) || flap == null) {
			base.add_child (builder, child, type);

			return;
		}

		var widget = child as Gtk.Widget;

		if (type == "titlebar")
			flap.flap = widget;
		else
			flap.content = widget;
	}

	[GtkCallback]
	private void motion_cb (double x, double y) {
		if (!autohide)
			return;

		if (y <= SHOW_HEADERBAR_DISTANCE)
			show_ui ();

		on_cursor_moved ();
	}

	private void show_ui () {
		if (ui_timeout_id != -1) {
			Source.remove (ui_timeout_id);
			ui_timeout_id = -1;
		}

		if (!is_fullscreen)
			return;

		ui_timeout_id = Timeout.add (INACTIVITY_TIME_MILLISECONDS, hide_ui);
		flap.reveal_flap = true;
	}

	private bool hide_ui () {
		ui_timeout_id = -1;

		if (!is_fullscreen)
			return false;

		flap.reveal_flap = false;
		flap.grab_focus ();

		return false;
	}

	private void on_cursor_moved () {
		if (cursor_timeout_id != -1) {
			Source.remove (cursor_timeout_id);
			cursor_timeout_id = -1;
		}

		cursor_timeout_id = Timeout.add (INACTIVITY_TIME_MILLISECONDS, on_inactivity);
		show_cursor (true);
	}

	private bool on_inactivity () {
		cursor_timeout_id = -1;

		show_cursor (false);

		return false;
	}

	private void on_restore () {
		if (ui_timeout_id != -1) {
			Source.remove (ui_timeout_id);
			ui_timeout_id = -1;
		}

		if (cursor_timeout_id != -1) {
			Source.remove (cursor_timeout_id);
			cursor_timeout_id = -1;
		}

		// This is needed when restoring via a keyboard shortcut when the
		// titlebar is concealed.
		flap.reveal_flap = true;

		on_cursor_moved ();
	}

	private void show_cursor (bool show) {
		if (show == (cursor == null))
			return;

		if (!show) {
			cursor = new Gdk.Cursor.from_name ("none", null);
		}
		else
			cursor = null;
	}
}
