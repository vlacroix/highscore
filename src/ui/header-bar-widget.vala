// This file is part of Highscore. License: GPL-3.0+.

public interface Highscore.HeaderBarWidget : Gtk.Widget {
	public abstract bool popover_visible { get; }
}
